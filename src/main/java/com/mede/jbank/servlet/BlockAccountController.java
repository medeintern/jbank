package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.util.RoutingUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/blockAccount")
public class BlockAccountController extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RoutingUtils.forwardToPage("blockAccount", request, response);
    }
}
