package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.TransactionDto;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.TransactionService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.WebUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/transactions")
public class TransactionsController extends HttpServlet {

    private transient TransactionService transactionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        transactionService = serviceContext.getService(TransactionService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        List<TransactionDto> transactionDtoList = transactionService.getListTransactions(principal.getId());
        request.setAttribute("transactionDtoList", transactionDtoList);
        RoutingUtils.forwardToPage("transactions", request, response);
    }
}
