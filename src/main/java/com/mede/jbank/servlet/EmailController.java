package com.mede.jbank.servlet;

import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CustomerService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.StringValidator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(urlPatterns = "/emailController")
public class EmailController extends HttpServlet {

    private static final String MESSAGE_ERROR = "messageError";
    private transient CustomerService customerService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String code = request.getParameter("code");

        if (StringValidator.isBlank(code)) {
            request.setAttribute(MESSAGE_ERROR, "Something went wrong. Call our customer department, please.");
            RoutingUtils.forwardToPage("login", request, response);
            return;
        }

        customerService.activateByCode(UUID.fromString(code));
        response.sendRedirect("/login");
    }
}
