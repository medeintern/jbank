package com.mede.jbank.servlet;

import com.mede.jbank.util.WebUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/logout")
public class LogOutController extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie userTokenCookie = WebUtils.findCookie(request, WebUtils.USER_TOKEN_COOKIE_NAME);
        WebUtils.deleteCookie(response, userTokenCookie);
        response.sendRedirect("/startPage");
    }
}
