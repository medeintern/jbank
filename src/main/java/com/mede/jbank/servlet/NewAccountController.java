package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.util.PropertyUtil;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.WebUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/newAccount")
public class NewAccountController extends HttpServlet {

    private static final String MAX_ACCOUNTS_PROPERTY = "max.number.customer.accounts";
    private static final Long MAX_ACCOUNTS = PropertyUtil.getLong(MAX_ACCOUNTS_PROPERTY);
    private transient CurrencyService currencyService;
    private transient AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        currencyService = serviceContext.getService(CurrencyService.class);
        accountService = serviceContext.getService(AccountService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        Map<String, Boolean> map = currencyService.findAvailableCurrenciesForNewAccount(principal.getId(), MAX_ACCOUNTS);
        request.setAttribute("currencyMap", map);
        RoutingUtils.forwardToPage("newAccount", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accountCurrencyName = request.getParameter("accountCurrency");
        Currency currency = currencyService.findCurrencyByName(accountCurrencyName);
        if (currency == null || currency.getIsBase()) {
            returnDoGetWithMessage(request, response, "Error. It is impossible to create an account!");
            return;
        }
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        Long numberAccountsInCurrency = accountService.getNumberAccountsByCurrency(principal.getId(), currency.getId());
        if (numberAccountsInCurrency >= MAX_ACCOUNTS) {
            String message = "Error. It is impossible to create an account in " + accountCurrencyName + "!";
            returnDoGetWithMessage(request, response, message);
            return;
        }
        Account newAccount = new Account();
        newAccount.setCustomerId(principal.getId());
        newAccount.setCurrencyId(currency.getId());
        newAccount.setAmount(BigDecimal.valueOf(0));
        newAccount.setIsBlocked(false);
        Account savedAccount = accountService.saveAccount(newAccount, MAX_ACCOUNTS);
        if (savedAccount == null) {
            returnDoGetWithMessage(request, response, "Error. It is impossible to create an account!");
            return;
        }
        response.sendRedirect("/secured/balance");
    }

    private void returnDoGetWithMessage(HttpServletRequest request, HttpServletResponse response, String message) throws ServletException, IOException {
        request.setAttribute("Message", message);
        doGet(request, response);
    }
}
