package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.CustomerService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AccessRole(roles = Role.MANAGER)
@WebServlet("/secured/admin/customerChangeBlocked")
public class CustomerBlockedController extends HttpServlet {

    private transient CustomerService customerService;
    private transient AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
        accountService = serviceContext.getService(AccountService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("id"));

        if (req.getParameter("customerBlocked") != null) {
            boolean isBlocked = Boolean.parseBoolean(req.getParameter("blocked"));
            customerService.changeBlocked(id, !isBlocked);
            req.getRequestDispatcher("/secured/admin/customerManagement").forward(req, resp);
            return;
        }

        if (req.getParameter("accountBlocked") != null) {
            boolean isBlocked = Boolean.parseBoolean(req.getParameter("blocked"));
            accountService.changeBlocked(id, !isBlocked);
            req.getRequestDispatcher("/secured/admin/accountsBlocked").forward(req, resp);
        }
    }
}
