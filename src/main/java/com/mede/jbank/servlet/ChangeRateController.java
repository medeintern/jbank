package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.Rate;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.service.impl.RateService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;

@AccessRole(roles = {Role.MANAGER})
@WebServlet(urlPatterns = "/secured/admin/newRate")
public class ChangeRateController extends HttpServlet {

    private transient CurrencyService currencyService;
    private transient RateService rateService;
    private transient JbValidator validator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        currencyService = serviceContext.getService(CurrencyService.class);
        rateService = serviceContext.getService(RateService.class);
        validator = JbValidatorFactory.INSTANCE.getValidator();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Currency> currencies = currencyService.findAllCurrencies();
        request.setAttribute("currencies", currencies);
        RoutingUtils.forwardToPage("newRatePage", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long currencyId = Long.valueOf(request.getParameter("id"));
        Double rate = Double.valueOf(request.getParameter("rate").trim());
        Rate newRate = new Rate();
        newRate.setCurrencyId(currencyId);
        newRate.setRate(rate);
        newRate.setEndDate(null);
        newRate.setStartDate(LocalDateTime.now(ZoneOffset.UTC));
        Set<String> errorSet = validator.validateOutSetString(newRate);
        if (!errorSet.isEmpty()) {
            request.setAttribute("errors", errorSet);
            doGet(request, response);
            return;
        }
        Rate savedRate = rateService.insertNextRateAndSetEndPreviousSetCurrencyRate(newRate);
        if (savedRate != null) {
            response.sendRedirect("/secured/admin/newRate");
            return;
        }
        errorSet.add("Impossible change rate");
        request.setAttribute("errors", errorSet);
        doGet(request, response);

    }
}
