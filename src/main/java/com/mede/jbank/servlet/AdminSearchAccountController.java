package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.util.StringValidator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@AccessRole(roles = Role.MANAGER)
@WebServlet("/secured/admin/accountsBlocked")
public class AdminSearchAccountController extends HttpServlet {

    private transient AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        accountService = serviceContext.getService(AccountService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!StringValidator.isEmpty(req.getParameter("customerId"))) {
            Long customerId = Long.valueOf(req.getParameter("customerId"));
            List<AccountDto> accountDtoList = accountService.searchAccountsByCustomerId(customerId);
            req.setAttribute("accountDtoList", accountDtoList);
        }
        req.getRequestDispatcher("/secured/admin/customerManagement").forward(req, resp);
    }
}
