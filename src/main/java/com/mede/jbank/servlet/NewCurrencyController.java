package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@AccessRole(roles = {Role.MANAGER})
@WebServlet(urlPatterns = "/secured/admin/newCurrency")
public class NewCurrencyController extends HttpServlet {

    private transient CurrencyService currencyService;
    private transient JbValidator validator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        currencyService = serviceContext.getService(CurrencyService.class);
        validator = JbValidatorFactory.INSTANCE.getValidator();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RoutingUtils.forwardToPage("addNewCurrency", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currencyName = request.getParameter("name").trim();
        String currencyAbbreviation = request.getParameter("abbreviation").trim();
        String currencyRate = request.getParameter("rate").trim();
        Double rate = Double.valueOf(currencyRate);
        Currency currency = new Currency();
        currency.setName(currencyName);
        currency.setAbbreviation(currencyAbbreviation);
        currency.setRate(rate);
        currency.setIsBase(false);
        Set<String> errorSet = validator.validateOutSetString(currency);
        if (!errorSet.isEmpty()) {
            request.setAttribute("errors", errorSet);
            request.setAttribute("currency", currency);
            doGet(request, response);
            return;
        }
        Currency savedCurrency = currencyService.insertNewCurrency(currency);
        if (savedCurrency != null) {
            response.sendRedirect("/currencies");
            return;
        }
        request.setAttribute("errors", "Impossible create currency.");
        request.setAttribute("currency", currency);
        doGet(request, response);
    }
}
