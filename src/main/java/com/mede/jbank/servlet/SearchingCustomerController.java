package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.StringValidator;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Log4j2
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/searching")
public class SearchingCustomerController extends HttpServlet {

    private transient AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        accountService = serviceContext.getService(AccountService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RoutingUtils.forwardToPage("searchingCustomer", req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String accFrom = req.getParameter("accFrom");
        String accTo = req.getParameter("accTo");
        if (accFrom != null) {
            req.setAttribute("accountFrom", accFrom);
            req.setAttribute("isClaim", true);
            RoutingUtils.forwardToPage("newTransaction", req, resp);
            return;
        }
        if (accTo != null) {
            req.setAttribute("accountTo", accTo);
            RoutingUtils.forwardToPage("newTransaction", req, resp);
            return;
        }

        String login = req.getParameter("login").toLowerCase(Locale.ENGLISH).trim();
        String name = req.getParameter("firstName").toLowerCase(Locale.ENGLISH).trim();
        String surname = req.getParameter("lastName").toLowerCase(Locale.ENGLISH).trim();
        String accountNumber = req.getParameter("account").trim();

        List<AccountDto> accountDtoList;

        if (!(login.equals("") && name.equals("") && surname.equals("") && accountNumber.equals(""))) {
            if (!StringValidator.isEmpty(accountNumber)) {
                AccountDto accountDto = accountService.findAccountDtoByNumber(accountNumber);
                accountDtoList = accountDto != null ? Collections.singletonList(accountDto) : new ArrayList<>();
            } else {
                accountDtoList = accountService.searchAccountByCustomersFields(login, name, surname);
            }
            req.setAttribute("accountDtoList", accountDtoList);
        }
        RoutingUtils.forwardToPage("searchingCustomer", req, resp);
    }
}
