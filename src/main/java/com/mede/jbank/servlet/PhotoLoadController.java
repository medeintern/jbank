package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Photo;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.PhotoService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Log4j2
@WebServlet(urlPatterns = "/secured/photo")
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
public class PhotoLoadController extends HttpServlet {

    private transient PhotoService photoService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        photoService = serviceContext.getService(PhotoService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String parameter = request.getParameter("id");
        Long customerId = getLongFromString(parameter);
        Photo photo = photoService.findPhotoByCustomerId(customerId);
        if (photo == null) {
            response.sendRedirect("/img/picture.jpg");
            return;
        }
        byte[] byteArray = photo.getPhoto();
        if (byteArray == null || byteArray.length == 0) {
            response.sendRedirect("/img/picture.jpg");
            return;
        }
        try (OutputStream out = new BufferedOutputStream(response.getOutputStream())) {
            response.setContentType("image/jpg");
            out.write(byteArray);
            return;
        }
    }

    private Long getLongFromString(String input) {
        if (input == null) {
            return null;
        }
        try {
            Long number = Long.valueOf(input);
            return number;
        } catch (RuntimeException e) {
            log.error("Having trouble loading picture. Input parameter for Long.valueOf() parsing: " + input);
            return null;
        }
    }
}
