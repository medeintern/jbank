package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.NewTransactionDto;
import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.entity.enums.Type;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.TransactionService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.SumValidator;
import com.mede.jbank.util.WebUtils;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Log4j2
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/newTransaction")
public class NewTransactionController extends HttpServlet {

    private static final String AMOUNT_VIOLATION = "amountViolation";
    private static final String SENDER_VIOLATIONS = "senderViolations";
    private static final String RECIPIENT_VIOLATIONS = "recipientViolations";
    private transient JbValidator validator;
    private transient AccountService accountService;
    private transient TransactionService transactionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        validator = JbValidatorFactory.INSTANCE.getValidator();
        accountService = serviceContext.getService(AccountService.class);
        transactionService = serviceContext.getService(TransactionService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RoutingUtils.forwardToPage("newTransaction", request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        setRequestAttributes(request, "accountFrom", "accountTo", "sum");

        BigDecimal amount = SumValidator.checkSum(request.getParameter("sum"));
        Type transType = Type.valueOf(request.getParameter("transactionType"));

        Set<String> senderViolations = new HashSet<>();
        Set<String> recipientViolations = new HashSet<>();

        NewTransactionDto transactionDto = NewTransactionDto.builder()
                .senderAccountNumber(request.getParameter("accountFrom").trim())
                .recipientAccountNumber(request.getParameter("accountTo").trim())
                .amount(amount)
                .type(transType)
                .build();

        if (!checkIncomingData(transactionDto, senderViolations, recipientViolations, request)) {
            doGet(request, response);
            return;
        }

        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);

        switch (transType) {

            case CLAIM:
                if (!checkClaimInfo(transactionDto, principal, request, senderViolations, recipientViolations)) {
                    doGet(request, response);
                    return;
                }
                break;

            case TRANSFER:
                if (!checkTransferInfo(transactionDto, principal, request, senderViolations, recipientViolations)) {
                    doGet(request, response);
                    return;
                }
                break;

            default:
                throw new IllegalArgumentException();
        }

        Transaction transaction = transactionService.addTransaction(transactionDto);

        if (transaction != null) {
            request.setAttribute("transactionId", transaction.getId());
            request.setAttribute("whereFrom", request.getRequestURI());
            request.setAttribute("transDto", transactionService.findTransactionDetailDtoById(transaction.getId()));
            RoutingUtils.forwardToPage("transactionDetails", request, response);
            return;
        }

        String error = "Something went wrong. Try again, please.";
        setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
        doGet(request, response);
    }

    private boolean checkIncomingData(NewTransactionDto transactionDto, Set<String> senderViolations, Set<String> recipientViolations,
            HttpServletRequest request) {

        boolean result = true;
        String accountFrom = transactionDto.getSenderAccountNumber();
        String accountTo = transactionDto.getRecipientAccountNumber();

        if (!checkAccountNumber(accountFrom, SENDER_VIOLATIONS, senderViolations, request)) {
            result = false;
        }

        if (!checkAccountNumber(accountTo, RECIPIENT_VIOLATIONS, recipientViolations, request)) {
            result = false;
        }

        if (accountFrom.equals(accountTo)) {
            String error = "Accounts should be different.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        if (transactionDto.getAmount().equals(BigDecimal.ZERO)) {
            request.setAttribute(AMOUNT_VIOLATION, "Incorrect amount format - should be positive (not zero) number.");
            result = false;
        }

        return result;
    }

    private boolean checkTransferInfo(NewTransactionDto transactionDto, Principal principal, HttpServletRequest request,
            Set<String> senderViolations, Set<String> recipientViolations) {
        boolean result = true;
        Account senderAccount = accountService.findAccountByNumber(transactionDto.getSenderAccountNumber());
        Account recipientAccount = accountService.findAccountByNumber(transactionDto.getRecipientAccountNumber());

        if (senderAccount == null || senderAccount.getIsBlocked() || !senderAccount.getCustomerId().equals(principal.getId())) {
            String error = "Choose one of your working accounts.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        if (recipientAccount == null || recipientAccount.getIsBlocked()) {
            String error = "Account does not exist or blocked.";
            setErrorIntoViolationSet(recipientViolations, error, RECIPIENT_VIOLATIONS, request);
            result = false;
        }

        if (result && !senderAccount.getCustomerId().equals(recipientAccount.getCustomerId()) && !senderAccount.getCurrencyId().equals(recipientAccount.getCurrencyId())) {
            String error = "Transfer between different customers should be in same currency.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        if (senderAccount != null && senderAccount.getAmount().compareTo(transactionDto.getAmount()) < 0) {
            request.setAttribute(AMOUNT_VIOLATION, "Not enough money on your account.");
            result = false;
        }

        return result;
    }

    private boolean checkClaimInfo(NewTransactionDto transactionDto, Principal principal, HttpServletRequest request, Set<String> senderViolations,
            Set<String> recipientViolations) {

        boolean result = true;
        Account senderAccount = accountService.findAccountByNumber(transactionDto.getSenderAccountNumber());
        Account recipientAccount = accountService.findAccountByNumber(transactionDto.getRecipientAccountNumber());

        if (senderAccount == null || senderAccount.getIsBlocked()) {
            String error = "Account does not exist or blocked.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        if (result && senderAccount.getCustomerId().equals(principal.getId())) {
            String error = "Sender account should not be yours.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        if (recipientAccount == null || recipientAccount.getIsBlocked() || !recipientAccount.getCustomerId().equals(principal.getId())) {
            String error = "Choose one of your working accounts.";
            setErrorIntoViolationSet(recipientViolations, error, RECIPIENT_VIOLATIONS, request);
            result = false;
        }

        if (result && !senderAccount.getCurrencyId().equals(recipientAccount.getCurrencyId())) {
            String error = "Currencies do not match. Choose accounts with the same currencies.";
            setErrorIntoViolationSet(senderViolations, error, SENDER_VIOLATIONS, request);
            result = false;
        }

        return result;
    }

    private void setErrorIntoViolationSet(Set<String> setViolations, String error, String attributeName, HttpServletRequest request) {
        setViolations.add(error);
        request.setAttribute(attributeName, setViolations);
    }

    private boolean checkAccountNumber(String accountNumber, String attributeName, Set<String> setViolations, HttpServletRequest request) {
        boolean result = true;
        setViolations.addAll(validator.validateValueOutSetString(Account.class, "number", accountNumber));
        if (!setViolations.isEmpty()) {
            request.setAttribute(attributeName, setViolations);
            result = false;
        }
        return result;
    }

    private Map<String, String> createRequestMapParams(HttpServletRequest request, String... args) {
        Map<String, String> map = new HashMap<>();
        for (String p : args) {
            map.put(p, request.getParameter(p).trim());
        }
        return map;
    }

    private void setRequestAttributes(HttpServletRequest request, String... args) {
        Map<String, String> map = createRequestMapParams(request, args);
        for (Map.Entry<String, String> pair : map.entrySet()) {
            request.setAttribute(pair.getKey(), pair.getValue());
        }
    }
}
