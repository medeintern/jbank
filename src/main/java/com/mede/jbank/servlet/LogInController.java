package com.mede.jbank.servlet;

import com.mede.jbank.entity.Customer;
import com.mede.jbank.jwt.JwTokenHelper;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CustomerRoleService;
import com.mede.jbank.service.impl.CustomerService;
import com.mede.jbank.util.*;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

@Log4j2
@WebServlet(urlPatterns = "/login")
public class LogInController extends HttpServlet {

    private static final String MESSAGE_ERROR = "messageError";
    private transient CustomerService customerService;
    private transient CustomerRoleService customerRoleService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
        customerRoleService = serviceContext.getService(CustomerRoleService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("message") != null) {
            String message;
            message = request.getParameter("message");
            request.setAttribute(MESSAGE_ERROR, HttpStatus.valueOf(message).getMessage());
        }
        RoutingUtils.forwardToPage("login", request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login").toLowerCase(Locale.ENGLISH);
        request.setAttribute("inputLogin", login);
        String password = request.getParameter("password");

        if (!StringValidator.isBlank(login) && !StringValidator.isBlank(password)) {

            Customer customer = checkCustomer(login, password, request);
            if (Objects.nonNull(customer)) {
                Principal principal = new Principal();
                principal.setLogin(customer.getLogin());
                principal.setId(customer.getId());
                principal.setName(customer.getName());
                principal.setRoles(customerRoleService.findRolesByCustomerId(customer.getId()));
                String userToken = JwTokenHelper.createJwt(principal);
                WebUtils.setCookie(WebUtils.USER_TOKEN_COOKIE_NAME, userToken, response);
                response.sendRedirect("/secured/balance");

            } else {
                this.doGet(request, response);
            }
        } else {
            log.warn("Empty Login or Password");
            request.setAttribute(MESSAGE_ERROR, "Login and Password should be filed.");
            this.doGet(request, response);
        }
    }

    private Customer checkCustomer(String login, String password, HttpServletRequest request) {
        Customer customer = customerService.findCustomerByLogin(login);

        if (customer == null) {
            log.warn("Customer with login \"" + login + "\" does not exist.");
            request.setAttribute(MESSAGE_ERROR, "Customer with this login does not exist");
            return null;
        } else if (PasswordHash.findHash(login, password).equals(customer.getPassword())) {
            return customer;
        } else {
            log.warn("Wrong password for login \"" + login + "\".");
            request.setAttribute(MESSAGE_ERROR, "Wrong password. Try again! ");
            return null;
        }
    }
}
