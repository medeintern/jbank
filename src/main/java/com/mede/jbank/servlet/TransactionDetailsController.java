package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.entity.enums.State;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.TransactionService;
import com.mede.jbank.util.RoutingUtils;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/transactionDetails")
public class TransactionDetailsController extends HttpServlet {

    private static final String NEW_TRANSACTION_URI = "/secured/newTransaction";
    private static final String INCOMING_CLAIMS_URI = "/secured/incomingClaims";
    private static final String TRANSACTIONS_URI = "/secured/transactions";
    private transient TransactionService transactionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        transactionService = serviceContext.getService(TransactionService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RoutingUtils.forwardToPage("transactionDetails", req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long transactionId = Long.parseLong(req.getParameter("transactionId"));

        Transaction transaction = transactionService.getTransactionById(transactionId);
        String whereFrom = req.getParameter("whereFrom");

        if (req.getParameter("cancel") != null) {
            transactionService.changeStateTransaction(transactionId, State.DENIED);
            log.info("Transaction with id = {} was DENIED", transactionId);
        }

        if (req.getParameter("confirm") == null) {
            resp.sendRedirect(TRANSACTIONS_URI);
            return;
        }

        switch (transaction.getType()) {

            case TRANSFER:
                transactionService.transfer(transaction);
                break;

            case CLAIM:
                if (whereFrom.equals(NEW_TRANSACTION_URI)) {
                    transactionService.changeStateTransaction(transactionId, State.PENDING);
                }
                if (whereFrom.equals(INCOMING_CLAIMS_URI)) {
                    transactionService.changeStateTransaction(transactionId, State.CONFIRMED);
                    transactionService.transfer(transaction);
                }
                break;

            default:
                throw new IllegalArgumentException();
        }

        resp.sendRedirect(TRANSACTIONS_URI);
    }
}

