package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.WebUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/balance")
public class BalancePageController extends HttpServlet {

    private transient AccountService accountService;
    private transient CurrencyService currencyService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        accountService = serviceContext.getService(AccountService.class);
        currencyService = serviceContext.getService(CurrencyService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        List<AccountDto> accountDtoList = accountService.searchAccountsByCustomerId(principal.getId());
        request.setAttribute("accountDtoList", accountDtoList);

        Map<String, Double> currencyMap = currencyService.findMapCurrenciesWithRates();
        Currency baseCurrency = currencyService.findBaseCurrency();
        request.setAttribute("baseCurrency", baseCurrency.getAbbreviation());

        BigDecimal totalBalance = BigDecimal.valueOf(0);
        for (AccountDto account : accountDtoList) {
            if (!account.getIsBase()) {
                Double rate = currencyMap.get(account.getCurrencyAbbreviation());
                if (rate != null) {
                    totalBalance = totalBalance.add(account.getBalance().multiply(BigDecimal.valueOf(rate)));
                }
                continue;
            }
            totalBalance = totalBalance.add(account.getBalance());
        }

        request.setAttribute("totalBalance", totalBalance);
        RoutingUtils.forwardToPage("balance", request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accFrom = request.getParameter("accFrom");
        String accTo = request.getParameter("accTo");
        if (accTo != null) {
            request.setAttribute("isClaim", true);
            request.setAttribute("accountTo", accTo);
        }
        if (accFrom != null) {
            request.setAttribute("accountFrom", accFrom);
        }
        RoutingUtils.forwardToPage("newTransaction", request, response);
    }
}
