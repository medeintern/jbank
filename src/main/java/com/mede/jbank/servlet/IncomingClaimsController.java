package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.TransactionDto;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.entity.enums.State;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.TransactionService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.WebUtils;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Log4j2
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/incomingClaims")
public class IncomingClaimsController extends HttpServlet {
    private transient TransactionService transactionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        transactionService = serviceContext.getService(TransactionService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        List<TransactionDto> transactionDtoList = transactionService.getIncomingClaimsTransactions(principal.getId());
        request.setAttribute("transactionDtoList", transactionDtoList);
        RoutingUtils.forwardToPage("incomingClaims", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long transactionId = Long.parseLong(req.getParameter("transactionId"));

        if (req.getParameter("cancel") != null) {
            transactionService.changeStateTransaction(transactionId, State.DENIED);
            log.info("Incoming Claim Transaction was DENIED");
            doGet(req, resp);
            return;
        }
        if (req.getParameter("confirm") != null) {
            req.setAttribute("transactionId", transactionId);
            req.setAttribute("whereFrom", req.getRequestURI());
            req.setAttribute("transDto", transactionService.findTransactionDetailDtoById(transactionId));
            RoutingUtils.forwardToPage("transactionDetails", req, resp);
        }
    }
}
