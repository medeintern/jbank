package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.Photo;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CustomerService;
import com.mede.jbank.service.impl.PhotoService;
import com.mede.jbank.util.PasswordHash;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.StringValidator;
import com.mede.jbank.util.WebUtils;
import com.mede.jbank.validator.CustomerCheck;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

@WebServlet(urlPatterns = "/secured/profile")
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@MultipartConfig
public class ProfilePageController extends HttpServlet {

    private static final int MAX_PICTURE_SIZE = (2 * 1024 * 1024);//2 Mb
    private transient CustomerService customerService;
    private transient PhotoService photoService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
        photoService = serviceContext.getService(PhotoService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Customer customer = (Customer) request.getAttribute("customer");//case callback event with incorrect input, get user from attribute
        if (customer == null) {
            customer = loadCustomerFromContext(request);//normal case, call first, and get user from principal
        }
        customer.setPassword(null);// delete password from customer for security
        request.setAttribute("customer", customer);
        RoutingUtils.forwardToPage("profile", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Set<String> setErrors = new LinkedHashSet<>();
        Customer customer = checkDataFromForm(request, setErrors);
        Photo newPhoto = loadPhotoByForm(request, "photoFile", setErrors);

        if (!setErrors.isEmpty()) {
            request.setAttribute("error", setErrors);
            customer.setPassword(null);
            request.setAttribute("customer", customer);
            doGet(request, response);
            return;
        }
        customerService.updateCustomer(customer);
        savePhoto(newPhoto, customer.getId());
        response.sendRedirect("/startPage");
    }

    /* The method returns the instance Customer for further saving or returning to the form if there are input errors.
    The method also returns validation error messages to the destinationSet. */
    private Customer checkDataFromForm(HttpServletRequest request, Set<String> destinationSet) {
        String firstName = request.getParameter("firstName").trim();
        String lastName = request.getParameter("lastName").trim();
        String email = request.getParameter("email").trim();
        String address = request.getParameter("address").trim();
        String currentPassword = request.getParameter("currentPassword").trim();
        String newPassword = request.getParameter("newPassword").trim();
        String repeatPassword = request.getParameter("retypePassword").trim();

        JbValidator validator = JbValidatorFactory.INSTANCE.getValidator();
        Customer customer = checkPasswords(loadCustomerFromContext(request), currentPassword, newPassword, repeatPassword, destinationSet, validator);
        if (!customer.getEmail().equals(email)) {
            Customer customerCheckEmail = customerService.findCustomerByEmail(email);
            if (customerCheckEmail != null) {
                destinationSet.add("This email already exists.");
            }
        }
        customer.setEmail(email);
        customer.setName(firstName);
        customer.setSurname(lastName);
        customer.setAddress(address);
        destinationSet.addAll(validator.validateOutSetString(customer, CustomerCheck.class));
        return customer;
    }

    Customer checkPasswords(Customer customer, String currentPassword, String newPassword, String repeatPassword,
            Set<String> destinationSet, JbValidator validator) {
        boolean correctCurrentPassword = customer.getPassword().equals(PasswordHash.findHash(customer.getLogin(), currentPassword));
        boolean equalsNewPasswords = StringValidator.equals(newPassword, repeatPassword) && StringValidator.notEmpty(newPassword);
        if (!correctCurrentPassword) {
            destinationSet.add("Wrong current password");
        }
        if (!equalsNewPasswords && (StringValidator.notEmpty(newPassword) || StringValidator.notEmpty(repeatPassword))) {
            destinationSet.add("New password and retype different");
        }
        if (correctCurrentPassword && equalsNewPasswords && StringValidator.equals(currentPassword, newPassword)) {
            destinationSet.add("New password and current are the same");
        }
        if (correctCurrentPassword && equalsNewPasswords && !StringValidator.equals(currentPassword, newPassword)) {
            customer.setPassword(PasswordHash.findHash(customer.getLogin(), newPassword));
            destinationSet.addAll(validator.validateValueOutSetString(Customer.class, "password", newPassword));
        }
        return customer;
    }

    private void savePhoto(Photo newPhoto, Long customerId) {
        if (newPhoto == null || newPhoto.getPhoto() == null) {
            return;
        }
        Photo currentPhoto = photoService.findPhotoByCustomerId(customerId);
        if (currentPhoto == null) {
            newPhoto.setCustomerId(customerId);
            photoService.addPhoto(newPhoto);
        } else {
            currentPhoto.setPhoto(newPhoto.getPhoto());
            photoService.updatePhoto(currentPhoto);
        }
    }

    private Photo loadPhotoByForm(HttpServletRequest request, String formName, Set<String> setErrors) throws IOException, ServletException {
        Part filePart = request.getPart(formName);
        if (filePart.getSize() == 0) {
            return null;
        }
        if (!filePart.getContentType().contains("image")) {
            setErrors.add("Choosen file should be image");
            return null;
        }
        if (filePart.getSize() > MAX_PICTURE_SIZE) {
            setErrors.add("Size photo shouldn't be more than 2 Mb");
            return null;
        }
        BufferedImage image = ImageIO.read(filePart.getInputStream());
        BufferedImage result;
        byte[] photoArray;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            result = new BufferedImage(
                    image.getWidth(),
                    image.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", out);
            photoArray = out.toByteArray();
        }
        Photo newPhoto = new Photo();
        newPhoto.setPhoto(photoArray);
        return newPhoto;
    }

    private Customer loadCustomerFromContext(HttpServletRequest request) {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        String login = null;
        if (principal != null) {
            login = principal.getLogin();
        }
        Customer customer = customerService.findCustomerByLogin(login);
        return customer;
    }
}
