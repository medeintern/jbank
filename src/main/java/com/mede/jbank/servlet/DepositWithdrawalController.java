package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.dto.BaseAccountDto;
import com.mede.jbank.dto.NewTransactionDto;
import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.entity.enums.Type;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.TransactionService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.SumValidator;
import com.mede.jbank.util.WebUtils;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Log4j2
@AccessRole(roles = {Role.CUSTOMER, Role.MANAGER})
@WebServlet(urlPatterns = "/secured/depositWithdrawal")
public class DepositWithdrawalController extends HttpServlet {

    private static final String AMOUNT_VIOLATION = "amountViolation";
    private static final String ACCOUNT_VIOLATIONS = "accountViolations";
    private transient JbValidator validator;
    private transient AccountService accountService;
    private transient TransactionService transactionService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        accountService = serviceContext.getService(AccountService.class);
        validator = JbValidatorFactory.INSTANCE.getValidator();
        transactionService = serviceContext.getService(TransactionService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        BaseAccountDto baseAccountDto = accountService.findBaseAccountDtoByCustomerId(principal.getId());
        request.setAttribute("baseAccountDto", baseAccountDto);
        RoutingUtils.forwardToPage("depositWithdrawal", request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String accountNumber = request.getParameter("account").trim();
        String sum = request.getParameter("sum");

        boolean correct = true;
        Set<String> setViolations = new HashSet<>();

        if (!checkAccountNumber(accountNumber, request, setViolations) || !checkAccountExistence(accountNumber, request, setViolations)) {
            correct = false;
        }

        BigDecimal requestedAmount = SumValidator.checkSum(sum);
        if (requestedAmount.equals(BigDecimal.ZERO)) {
            request.setAttribute(AMOUNT_VIOLATION, "Incorrect amount format - should be positive (not zero) number.");
            correct = false;
        }

        if (!correct) {
            request.setAttribute("account", accountNumber);
            request.setAttribute("sum", sum);
            doGet(request, response);
            return;
        }

        Type transType = Type.valueOf(request.getParameter("transactionType"));
        String adminBaseAccountNumber = accountService.findAdminBaseAccountNumber();
        NewTransactionDto transactionDto;
        switch (transType) {
            case DEPOSIT:
                transactionDto = NewTransactionDto
                        .builder()
                        .senderAccountNumber(adminBaseAccountNumber)
                        .recipientAccountNumber(accountNumber)
                        .type(transType)
                        .amount(requestedAmount)
                        .build();
                break;
            case WITHDRAW:
                requestedAmount = requestedAmount.negate();
                transactionDto = NewTransactionDto
                        .builder()
                        .senderAccountNumber(accountNumber)
                        .recipientAccountNumber(adminBaseAccountNumber)
                        .type(transType)
                        .amount(requestedAmount)
                        .build();
                break;
            default:
                throw new IllegalArgumentException();
        }

        Transaction transaction = transactionService.addTransaction(transactionDto);
        transactionService.depositWithdraw(transaction);
        doGet(request, response);
    }

    private boolean checkAccountExistence(String number, HttpServletRequest request, Set<String> setViolations) {
        boolean result = true;

        if (accountService.findWorkingAccountByNumber(number) == null) {
            setViolations.add("Account does not exist or blocked");
            request.setAttribute(ACCOUNT_VIOLATIONS, setViolations);
            result = false;
        }

        if (result && !accountService.isBaseAccount(number)) {
            setViolations.add("Account should be base.");
            request.setAttribute(ACCOUNT_VIOLATIONS, setViolations);
            result = false;
        }

        return result;
    }

    private boolean checkAccountNumber(String accountNumber, HttpServletRequest request, Set<String> setViolations) {
        boolean result = true;
        setViolations.addAll(validator.validateValueOutSetString(Account.class, "number", accountNumber));
        if (!setViolations.isEmpty()) {
            request.setAttribute(ACCOUNT_VIOLATIONS, setViolations);
            result = false;
        }
        return result;
    }
}
