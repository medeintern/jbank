package com.mede.jbank.servlet;

import com.mede.jbank.entity.Currency;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.util.RoutingUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


@WebServlet(urlPatterns = "/startPage")
public class StartPageController extends HttpServlet {

    private transient CurrencyService currencyService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        currencyService = serviceContext.getService(CurrencyService.class);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Map<String, Double> currencyList = currencyService.findMapCurrenciesWithRates();
        Currency baseCurrency = currencyService.findBaseCurrency();
        request.setAttribute("currencyList", currencyList);
        request.setAttribute("baseCurrency", baseCurrency);
        RoutingUtils.forwardToPage("startPage", request, response);
    }

}
