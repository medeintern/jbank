package com.mede.jbank.servlet;

import com.mede.jbank.email.EmailSender;
import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Gender;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CurrencyService;
import com.mede.jbank.service.impl.CustomerService;
import com.mede.jbank.util.PasswordHash;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@WebServlet("/registration")
public class RegistrationController extends HttpServlet {

    private transient JbValidator validator;
    private transient CustomerService customerService;
    private transient CurrencyService currencyService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        validator = JbValidatorFactory.INSTANCE.getValidator();
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
        currencyService = serviceContext.getService(CurrencyService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Gender> genderList = Arrays.stream(Gender.values())
                .filter(gender -> !gender.equals(Gender.UNDEFINED))
                .collect(Collectors.toList());
        req.setAttribute("genderList", genderList);
        RoutingUtils.forwardToPage("registrationPage", req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Customer customer = buildCustomerByParameters(req);
        String retypePassword = req.getParameter("retypePassword").trim();

        Set<String> failCustomerSet = validator.validateOutSetString(customer);

        if (!customer.getPassword().equals(retypePassword)) {
            failCustomerSet.add("Passwords don't match");
        }

        if (customerService.isExistCustomerByLoginOrEmail(customer)) {
            failCustomerSet.add("Login or email already exists");
        }

        if (!failCustomerSet.isEmpty()) {
            req.setAttribute("failCustomerSet", failCustomerSet);
            req.setAttribute("customer", customer);
            doGet(req, resp);
            return;
        }

        String encryptPassword = PasswordHash.findHash(customer.getLogin(), customer.getPassword());
        customer.setPassword(encryptPassword);
        Account account = new Account();
        account.setCurrencyId(currencyService.findBaseCurrency().getId());
        account.setAmount(BigDecimal.ZERO);
        account.setIsBlocked(false);

        Customer addedCustomer = customerService.addCustomer(customer, Role.CUSTOMER, account);
        if (addedCustomer != null) {
            EmailSender.sendEmail(addedCustomer);
            RoutingUtils.forwardToPage("email", req, resp);
            log.info("User registration successful");
            return;
        }
        req.setAttribute("failCustomerSet", Arrays.asList("Something went wrong. Try again, please."));
        doGet(req, resp);
    }

    private Customer buildCustomerByParameters(HttpServletRequest req) {
        Customer customer = new Customer();
        customer.setLogin(req.getParameter("login").toLowerCase(Locale.ENGLISH).trim());
        customer.setPassword(req.getParameter("password"));
        customer.setEmail(req.getParameter("email"));
        customer.setName(req.getParameter("firstName"));
        customer.setSurname(req.getParameter("lastName"));
        customer.setGender(Gender.valueOf(req.getParameter("gender")));
        customer.setAddress(req.getParameter("address"));

        LocalDate birthday = LocalDate.parse(req.getParameter("birthday"));
        customer.setBirthdate(birthday);

        customer.setRegistrationDate(LocalDateTime.now(ZoneOffset.UTC).withNano(0));

        customer.setIsBlocked(false);
        customer.setIsActive(false);

        return customer;
    }
}
