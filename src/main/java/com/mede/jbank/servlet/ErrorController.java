package com.mede.jbank.servlet;

import com.mede.jbank.exception.RoleAccessException;
import com.mede.jbank.exception.UnauthorizedException;
import com.mede.jbank.util.HttpStatus;
import com.mede.jbank.util.RoutingUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/error")
public class ErrorController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int status = resp.getStatus();
        Exception exception = (Exception) req.getAttribute("javax.servlet.error.exception");
        if (exception != null) {
            if (exception instanceof UnauthorizedException) {
                status = HttpStatus.UNAUTHORIZED.getStatus();
            }
            if (exception instanceof RoleAccessException) {
                status = HttpStatus.FORBIDDEN.getStatus();
            }
        }

        if (status == 401 || status == 403) {
            HttpStatus httpStatus = HttpStatus.findStatus(status);
            resp.sendRedirect("/login?message=" + httpStatus);
        } else {
            req.setAttribute("status", HttpStatus.findStatus(status));
            RoutingUtils.forwardToPage("error", req, resp);
        }
    }
}
