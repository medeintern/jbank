package com.mede.jbank.servlet;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.service.impl.CustomerService;
import com.mede.jbank.util.RoutingUtils;
import com.mede.jbank.util.StringValidator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AccessRole(roles = Role.MANAGER)
@WebServlet("/secured/admin/customerManagement")
public class CustomerManagementController extends HttpServlet {

    private transient CustomerService customerService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ServiceContext serviceContext = (ServiceContext) config.getServletContext().getAttribute(ServiceContext.class.getSimpleName());
        customerService = serviceContext.getService(CustomerService.class);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        Customer customer = (Customer) req.getAttribute("customer");
        if (!StringValidator.isEmpty(login) && customer == null) {
            customer = customerService.findCustomerByLogin(login);
            req.setAttribute("customer", customer);
        }
        RoutingUtils.forwardToPage("customerManagement", req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String email = req.getParameter("email");

        Customer customer;

        if (!StringValidator.isEmpty(login)) {
            customer = customerService.findCustomerByLogin(login);
            req.setAttribute("customer", customer);
            doGet(req, resp);
            return;
        }

        customer = customerService.findCustomerByEmail(email);

        req.setAttribute("customer", customer);
        doGet(req, resp);
    }
}
