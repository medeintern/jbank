package com.mede.jbank.util;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Log4j2
@UtilityClass
public class PasswordHash {

    private static final char[] SALT = {'p', 'a', 's', 'S', 'W', '@', 'r', 'D', ':', 'S', 'A', 'L', 't', '.'};

    public static String findHash(String login, String password) {
        StringBuilder sbLogin = new StringBuilder(login);
        sbLogin.reverse();
//      Set StringBuilder capacity = 390 for adding 3*128 symbols from SHA-512
        StringBuilder out = new StringBuilder(390);
        out.append(encryptString(password)).append(encryptString(String.valueOf(SALT))).append(encryptString(sbLogin.toString()));
        return encryptString(out.toString());
    }

    private static String encryptString(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger no = new BigInteger(1, messageDigest);
            StringBuilder sb = new StringBuilder(no.toString(16));
            while (sb.length() < 32) {
                sb.insert(0, "0");
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("Password encryption failed", e);
            throw new RuntimeException("Password encryption exception", e);
        }
    }
}

