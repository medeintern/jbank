package com.mede.jbank.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum HttpStatus {

    BAD_REQUEST(400, "The server has detected a syntax error in the client request"),
    UNAUTHORIZED(401, "Authentication is required to get the requested page"),
    FORBIDDEN(403, "No access rights to the requested page"),
    NOT_FOUND(404, "Page not found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    INTERNAL_ERROR(500, "Internal Server Error");

    private int status;

    private String message;

    public static HttpStatus findStatus(int status) {
        return Arrays.stream(HttpStatus.values())
                .filter(httpStatus -> httpStatus.status == status)
                .findFirst().orElse(null);
    }

}
