package com.mede.jbank.util;

import com.mede.jbank.jwt.JwTokenHelper;
import lombok.experimental.UtilityClass;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@UtilityClass
public final class WebUtils {

    public static final String USER_TOKEN_COOKIE_NAME = "USER_TOKEN";
    public static final String USER_PRINCIPAL = "PRINCIPAL";

    public static void setCookie(String name, String value, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(JwTokenHelper.TTL_MILLIS);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    public static Cookie findCookie(HttpServletRequest request, String cookieName) {
        Cookie[] ck = request.getCookies();

        if (Objects.nonNull(ck)) {
            for (Cookie cookie : ck) {
                if (cookie.getName().equals(cookieName)) {
                    if (!StringValidator.isBlank(cookie.getValue())) {
                        return cookie;
                    }
                }
            }
        }
        return null;
    }

    public static void deleteCookie(HttpServletResponse response, Cookie cookieToDelete) {
        if (Objects.nonNull(cookieToDelete)) {
            cookieToDelete.setMaxAge(0);
            cookieToDelete.setPath("/");
            cookieToDelete.setValue("");
            response.addCookie(cookieToDelete);
        }
    }
}
