package com.mede.jbank.util;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.SQLException;

@Log4j2
@UtilityClass
public class LiquibaseRunner {
    private static final String CHANGELOG_FILE = "src/main/resources/liquibase/outputChangeLog.xml";

    public static void initDb(Connection connection) throws Exception {
        Liquibase liquibase = null;
        try {
            Database database = DatabaseFactory.getInstance()
                    .findCorrectDatabaseImplementation(new JdbcConnection(connection));
            liquibase = new Liquibase(CHANGELOG_FILE, new FileSystemResourceAccessor(), database);
            liquibase.update(new Contexts());
        } catch (LiquibaseException e) {
            //todo logger
        } catch (Exception e) {
            log.error(e);
            throw new Exception("Exception in Liquibase", e);
        } finally {
            if (connection != null) {
                try {
                    connection.rollback();
                    connection.close();
                } catch (SQLException e) {
                    log.error(e);
                }
            }
        }
    }
}
