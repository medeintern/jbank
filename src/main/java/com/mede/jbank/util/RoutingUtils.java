package com.mede.jbank.util;

import lombok.experimental.UtilityClass;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@UtilityClass
public class RoutingUtils {

    private static final String PATH_TO_JSP = "/WEB-INF/view/";

    public static void forwardToPage(String jspPage, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(PATH_TO_JSP + jspPage + ".jsp").forward(request, response);
    }
}
