package com.mede.jbank.util;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;

@Log4j2
@UtilityClass
public class SumValidator {

    public static BigDecimal checkSum(String sum) {
        try {
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(sum));
            if (amount.compareTo(BigDecimal.ZERO) > 0) {
                return amount;
            }
        } catch (Exception nfe) {
            log.error("Incorrect amount format: {}", nfe.getMessage());
        }
        return BigDecimal.ZERO;
    }
}
