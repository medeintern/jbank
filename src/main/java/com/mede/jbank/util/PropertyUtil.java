package com.mede.jbank.util;

import lombok.Getter;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

@UtilityClass
@Log4j2
public class PropertyUtil {

    private static final String FILE_PATH = "application.properties";

    @Getter
    private static Map<String, String> propertiesMap = getAllProperties(FILE_PATH);

    public static String getString(String key) {
        return propertiesMap.get(key);
    }

    public static Long getLong(String key) {
        try {
            return Long.valueOf(propertiesMap.get(key));
        } catch (NullPointerException | NumberFormatException e) {
            return null;
        }
    }

    private static Map<String, String> getAllProperties(String file) {
        Map<String, String> map = new HashMap<>();
        try (InputStream fis = PropertyUtil.class.getClassLoader().getResourceAsStream(file)) {
            Properties properties = new Properties();
            properties.load(fis);
            map = properties.entrySet().stream()
            .collect(Collectors.toMap(p -> p.getKey().toString(), p -> p.getValue().toString()));
        } catch (FileNotFoundException e) {
            log.error(e);
        } catch (IOException e) {
            log.error(e);
        }
        return Collections.unmodifiableMap(map);
    }
}
