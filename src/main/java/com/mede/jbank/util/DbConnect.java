package com.mede.jbank.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Log4j2
@UtilityClass
public class DbConnect {
    private static final String FILEPATH_TO_DB_PROP = "src/main/resources/application.properties";
    private static final String DB_DRIVER_CLASS = "db.driver";
    private static final String DB_USERNAME = "db.user";
    private static final String DB_NAME = "db.name";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_HOST = "db.host";

    private static final HikariDataSource DATA_SOURCE;

    static {
        HikariConfig hikariConfig = new HikariConfig();
        try (FileInputStream fis = new FileInputStream(FILEPATH_TO_DB_PROP)) {
            Properties properties = new Properties();
            properties.load(fis);
            hikariConfig.setDriverClassName(properties.getProperty(DB_DRIVER_CLASS));

            final String DB_URL = properties.getProperty(DB_HOST) + "/" + properties.getProperty(DB_NAME);

            hikariConfig.setJdbcUrl(DB_URL);
            hikariConfig.setUsername(properties.getProperty(DB_USERNAME));
            hikariConfig.setPassword(properties.getProperty(DB_PASSWORD));

            hikariConfig.setMinimumIdle(5);
            hikariConfig.setMaximumPoolSize(30);

        } catch (IOException ex) {
            log.error(ex);
        }
        DATA_SOURCE = new HikariDataSource(hikariConfig);
    }

    public static DataSource getDataSource() {
        return DATA_SOURCE;
    }
}
