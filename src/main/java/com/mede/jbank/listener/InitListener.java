package com.mede.jbank.listener;

import com.mede.jbank.service.ServiceContext;
import com.mede.jbank.util.DbConnect;
import com.mede.jbank.util.LiquibaseRunner;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

@Log4j2
@WebListener
public class InitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        DataSource dataSource = DbConnect.getDataSource();
        servletContext.setAttribute(ServiceContext.class.getSimpleName(), new ServiceContext(dataSource));
        try {
            LiquibaseRunner.initDb(dataSource.getConnection());
            log.info("Successful database init");
        } catch (Exception e) {
            log.error("Database init failed", e);
            throw new RuntimeException("Database init exception", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
