package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Rate;
import com.mede.jbank.exception.TransactionImpossibleException;
import com.mede.jbank.repository.CurrencyRepository;
import com.mede.jbank.repository.RateRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

@AllArgsConstructor
@Log4j2
public class RateService implements DbService {

    private final Jdbi jdbi;

    public List<Rate> findAllRates() {
        return jdbi.withExtension(RateRepository.class, RateRepository::findAllRates);
    }

    public Rate insertNextRateAndSetEndPreviousSetCurrencyRate(Rate rate) {
        return jdbi.inTransaction(handle -> {
            RateRepository rateRepository = handle.attach(RateRepository.class);
            CurrencyRepository currencyRepository = handle.attach(CurrencyRepository.class);
            Rate savedRate = null;
            try {
                if (!rateRepository.updateLastRate(rate).getEndDate().equals(rate.getStartDate())) {
                    throw new TransactionImpossibleException();
                }
                savedRate = rateRepository.insert(rate);
                if (savedRate == null) {
                    throw new TransactionImpossibleException();
                }
                if (!currencyRepository.updateCurrencyRate(rate.getCurrencyId(), rate.getRate())) {
                    throw new TransactionImpossibleException();
                }
            } catch (TransactionImpossibleException exc) {
                log.error("It's impossible to change rate");
                return null;
            }
            return savedRate;
        });
    }

}
