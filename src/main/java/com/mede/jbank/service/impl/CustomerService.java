package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.repository.AccountRepository;
import com.mede.jbank.repository.CustomerRepository;
import com.mede.jbank.repository.CustomerRoleRepository;
import com.mede.jbank.service.DbService;

import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

@AllArgsConstructor
public class CustomerService implements DbService {

    private final Jdbi jdbi;

    public Customer findCustomerByLogin(String login) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.findByLogin(login.toLowerCase(Locale.ENGLISH).trim()));
    }

    public Customer findCustomerById(Long id) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.findById(id));
    }

    public Customer findCustomerByEmail(String email) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.findByEmail(email));
    }

    public boolean activateByCode(UUID code) {
        return jdbi.inTransaction(handle -> {
            CustomerRepository customerRepo = handle.attach(CustomerRepository.class);
            customerRepo.activateByCode(code);
            return customerRepo.deleteCode(code);
        });
    }

    public boolean isExistCustomerByLoginOrEmail(Customer customer) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.isExistCustomerByLoginOrEmail(customer));
    }

    public List<Customer> findListCustomers() {
        return jdbi.withExtension(CustomerRepository.class, CustomerRepository::getListCustomers);
    }

    public Customer addCustomer(Customer customer, Role role, Account account) {
        return jdbi.inTransaction(handle -> {
            CustomerRepository customerRepo = handle.attach(CustomerRepository.class);
            CustomerRoleRepository customerRoleRepo = handle.attach(CustomerRoleRepository.class);
            AccountRepository accountRepository = handle.attach(AccountRepository.class);
            Customer newCustomer = customerRepo.insert(customer);
            account.setCustomerId(newCustomer.getId());
            accountRepository.saveAccount(account);
            customerRoleRepo.insert(newCustomer.getId(), role);
            return newCustomer;
        });
    }

    public boolean updateCustomer(Customer customer) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.updateCustomer(customer));
    }

    public boolean changeBlocked(Long id, boolean isBlocked) {
        return jdbi.withExtension(CustomerRepository.class, handle -> handle.changeBlocked(id, isBlocked));
    }
}
