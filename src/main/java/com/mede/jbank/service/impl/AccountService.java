package com.mede.jbank.service.impl;

import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.dto.BaseAccountDto;
import com.mede.jbank.entity.Account;
import com.mede.jbank.repository.AccountRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

@Log4j2
@AllArgsConstructor
public class AccountService implements DbService {

    private final Jdbi jdbi;

    public List<AccountDto> searchAccountByCustomersFields(String login, String name, String surname) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.searchAccountByCustomerFields(login, name, surname));
    }

    public AccountDto findAccountDtoByNumber(String number) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findAccountDtoByNumber(number));
    }

    public Account findAccountById(Long id) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findAccountById(id));
    }

    public Account findAccountByNumber(String number) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findAccountByNumber(number));
    }

    public boolean isBaseAccount(String number) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.isBaseAccount(number));
    }

    public List<AccountDto> searchAccountsByCustomerId(Long id) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.searchAccountsByCustomerId(id));
    }

    public boolean changeBlocked(Long id, boolean isBlocked) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.changeBlocked(id, isBlocked));
    }

    public BaseAccountDto findBaseAccountDtoByCustomerId(Long id) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findBaseAccountDtoByCustomerId(id));
    }

    public Long findWorkingAccountByNumber(String number) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findWorkingAccountByNumber(number));
    }

    public String findAdminBaseAccountNumber() {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findAdminBaseAccountNumber());
    }

    public Long getNumberAccountsByCurrency(Long customerId, Long currencyId) {
        return jdbi.withExtension(AccountRepository.class, handle -> handle.findNumberCurrenciesByCustomer(customerId, currencyId));
    }

    public Account saveAccount(Account account, Long maxNumberAccounts) {
        return jdbi.inTransaction(handle -> {
            AccountRepository accountRepository = handle.attach(AccountRepository.class);
            Long numberAccounts = accountRepository.findNumberCurrenciesByCustomer(account.getCustomerId(), account.getCurrencyId());
            if (numberAccounts >= maxNumberAccounts) {
                log.error("Can't save account in database.");
                return null;
            }
            Account savedAccount = accountRepository.saveAccount(account);
            return savedAccount;

        });
    }
}
