package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Photo;
import com.mede.jbank.repository.PhotoRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

@AllArgsConstructor
public class PhotoService implements DbService {

    private final Jdbi jdbi;

    public Photo findPhotoByCustomerId(final Long customerId) {
        return jdbi.withExtension(PhotoRepository.class, handle -> handle.findByCustomerId(customerId));
    }

    public Photo addPhoto(Photo photo) {
        return jdbi.withExtension(PhotoRepository.class, handle -> handle.insert(photo));
    }

    public boolean updatePhoto(Photo photo) {
        return jdbi.withExtension(PhotoRepository.class, handle -> handle.updatePhoto(photo));
    }

}
