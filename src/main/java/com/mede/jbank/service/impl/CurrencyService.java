package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.Rate;
import com.mede.jbank.exception.TransactionImpossibleException;
import com.mede.jbank.repository.CurrencyRepository;
import com.mede.jbank.repository.RateRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Log4j2
public class CurrencyService implements DbService {

    private final Jdbi jdbi;

    public Map<String, Double> findMapCurrenciesWithRates() {
        return jdbi.withExtension(CurrencyRepository.class, CurrencyRepository::findMapCurrenciesWithRates);
    }

    public Currency findBaseCurrency() {
        return jdbi.withExtension(CurrencyRepository.class, CurrencyRepository::findBaseCurrency);
    }

    public List<Currency> findAllCurrencies() {
        return jdbi.withExtension(CurrencyRepository.class, CurrencyRepository::findAllCurrencies);
    }

    public Currency insertNewCurrency(Currency currency) {
        return jdbi.inTransaction(handle -> {
            CurrencyRepository currencyRepository = handle.attach(CurrencyRepository.class);
            RateRepository rateRepository = handle.attach(RateRepository.class);
            Currency savedCurrency = null;
            try {
                savedCurrency = currencyRepository.insert(currency);
                if (savedCurrency == null) {
                    throw new TransactionImpossibleException();
                }
                Rate rate = new Rate();
                rate.setCurrencyId(savedCurrency.getId());
                rate.setRate(savedCurrency.getRate());
                rate.setStartDate(LocalDateTime.now(ZoneOffset.UTC));
                rate.setEndDate(null);
                Rate savedRate = rateRepository.insert(rate);
                if (savedRate == null) {
                    throw new TransactionImpossibleException();
                }
            } catch (TransactionImpossibleException ex) {
                log.error("Impossible save currency {}", currency);
                return null;
            }
            return savedCurrency;
        });
    }

    public Map<String, Boolean> findAvailableCurrenciesForNewAccount(Long customerId, Long maxAccountsNumber) {
        return jdbi.withExtension(CurrencyRepository.class, handle -> handle.findAvailableCurrenciesForNewAccount(customerId, maxAccountsNumber));
    }

    public Currency findCurrencyByName(String name) {
        return jdbi.withExtension(CurrencyRepository.class, handle -> handle.findCurrencyByName(name));
    }

    public Currency findCurrencyById(Long id) {
        return jdbi.withExtension(CurrencyRepository.class, handle -> handle.findCurrencyById(id));
    }
}
