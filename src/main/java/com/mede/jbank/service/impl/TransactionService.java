package com.mede.jbank.service.impl;

import com.mede.jbank.dto.NewTransactionDto;
import com.mede.jbank.dto.TransactionDetailDto;
import com.mede.jbank.dto.TransactionDto;
import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.State;
import com.mede.jbank.exception.NoMoneyException;
import com.mede.jbank.repository.AccountRepository;
import com.mede.jbank.repository.CurrencyRepository;
import com.mede.jbank.repository.TransactionRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Log4j2
@AllArgsConstructor
public class TransactionService implements DbService {

    private final Jdbi jdbi;

    public Transaction addTransaction(NewTransactionDto transactionDto) {
        return jdbi.inTransaction(handle -> {
            AccountRepository accountRepository = handle.attach(AccountRepository.class);
            TransactionRepository transactionRepository = handle.attach(TransactionRepository.class);

            Long senderId = accountRepository.findAccountIdByNumber(transactionDto.getSenderAccountNumber());
            Long recipientId = accountRepository.findAccountIdByNumber(transactionDto.getRecipientAccountNumber());

            Transaction transaction = new Transaction();
            transaction.setSenderAccountId(senderId);
            transaction.setRecipientAccountId(recipientId);
            transaction.setTransactionDate(LocalDateTime.now(ZoneOffset.UTC).withNano(0));
            transaction.setAmount(transactionDto.getAmount());
            transaction.setRateSend(accountRepository.getRateByAccountId(senderId));
            transaction.setRateRecipient(accountRepository.getRateByAccountId(recipientId));
            transaction.setState(State.CREATED);
            transaction.setType(transactionDto.getType());

            return transactionRepository.insert(transaction);
        });
    }

    public List<TransactionDto> getListTransactions(Long id) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.getListTransactionsByCustomerId(id));
    }

    public List<TransactionDto> getPendingClaimsTransactions(Long accountId) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.getPendingClaimsTransactions(accountId));
    }

    public boolean changeStateTransaction(Long id, State state) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.changeState(id, state));
    }

    public List<TransactionDto> getIncomingClaimsTransactions(Long accountId) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.getIncomingClaimsTransactions(accountId));
    }

    public Transaction getTransactionById(Long id) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.findTransactionById(id));
    }

    public Transaction depositWithdraw(Transaction transaction) {
        jdbi.withExtension(TransactionRepository.class, handle -> handle.changeState(transaction.getId(), State.STARTED)
        );
        try {
            return jdbi.inTransaction(handle -> {
                AccountRepository accountRepository = handle.attach(AccountRepository.class);
                TransactionRepository transactionRepository = handle.attach(TransactionRepository.class);
                Long accountId;
                switch (transaction.getType()) {

                    case DEPOSIT:
                        accountId = transaction.getRecipientAccountId();
                        break;

                    case WITHDRAW:
                        accountId = transaction.getSenderAccountId();
                        break;

                    default:
                        throw new IllegalArgumentException();
                }

                accountRepository.updateAmountAccount(accountId, transaction.getAmount());

                if (accountRepository.getAmountByAccountId(accountId).compareTo(BigDecimal.ZERO) < 0) {
                    log.warn("Not enough money on account, id = {}.", accountId);
                    throw new NoMoneyException("Not enough money on account");
                }
                transactionRepository.changeState(transaction.getId(), State.COMPLETED);

                return transaction;
            });
        } catch (NoMoneyException nme) {
            jdbi.withExtension(TransactionRepository.class, handle -> handle.changeState(transaction.getId(), State.FAILED));
        }
        return transaction;
    }

    public List<Transaction> getTransactionListByAccountId(Long accountId) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.getTransactionListByAccountId(accountId));
    }

    public Transaction transfer(Transaction transaction) {
        jdbi.withExtension(TransactionRepository.class, handle -> handle.changeState(transaction.getId(), State.STARTED)
        );
        try {
            return jdbi.inTransaction(handle -> {
                AccountRepository accountRepository = handle.attach(AccountRepository.class);
                TransactionRepository transactionRepository = handle.attach(TransactionRepository.class);
                CurrencyRepository currencyRepository = handle.attach(CurrencyRepository.class);

                Long senderAccountId = transaction.getSenderAccountId();
                Long recipientAccountId = transaction.getRecipientAccountId();

                BigDecimal neededAmount = transaction.getAmount();

                accountRepository.updateAmountAccount(senderAccountId, neededAmount.negate());
                Account senderAccount = accountRepository.findAccountById(senderAccountId);

                if (senderAccount.getAmount().compareTo(BigDecimal.ZERO) < 0) {
                    log.warn("Not enough money on account, id = {}.", senderAccountId);
                    throw new NoMoneyException("You do not have enough money on your account.");
                }

                Account recipientAccount = accountRepository.findAccountById(recipientAccountId);

                if (!senderAccount.getCurrencyId().equals(recipientAccount.getCurrencyId())) {
                    Double senderRate = currencyRepository.findRateByCurrencyId(senderAccount.getCurrencyId());
                    Double recipientRate = currencyRepository.findRateByCurrencyId(recipientAccount.getCurrencyId());
                    transaction.setRateSend(senderRate);
                    transaction.setRateRecipient(recipientRate);
                    transactionRepository.update(transaction);
                    neededAmount = convertCurrency(senderRate, recipientRate, neededAmount);
                }

                accountRepository.updateAmountAccount(recipientAccountId, neededAmount);
                transactionRepository.changeState(transaction.getId(), State.COMPLETED);

                return transaction;
            });
        } catch (NoMoneyException nme) {
            jdbi.withExtension(TransactionRepository.class, handle -> handle.changeState(transaction.getId(), State.FAILED));
        }
        return transaction;
    }

    private BigDecimal convertCurrency(Double senderRate, Double recipientRate, BigDecimal amount) {
        return amount.divide(BigDecimal.valueOf(senderRate), 2, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(recipientRate));
    }

    public TransactionDetailDto findTransactionDetailDtoById(Long id) {
        return jdbi.withExtension(TransactionRepository.class, handle -> handle.findTransactionDetailDtoById(id));
    }
}
