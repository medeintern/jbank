package com.mede.jbank.service.impl;

import com.mede.jbank.entity.CustomerRole;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.repository.CustomerRoleRepository;
import com.mede.jbank.service.DbService;
import lombok.AllArgsConstructor;
import org.jdbi.v3.core.Jdbi;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class CustomerRoleService implements DbService {

    private final Jdbi jdbi;

    public List<Role> findRolesByCustomerId(Long id) {
        return jdbi.withExtension(CustomerRoleRepository.class, handle -> handle.findRolesById(id))
                .stream()
                .map(CustomerRole::getRole)
                .collect(Collectors.toList());
    }
}
