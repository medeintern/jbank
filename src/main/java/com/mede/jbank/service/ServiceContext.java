package com.mede.jbank.service;

import com.mede.jbank.service.impl.*;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.config.ConfigRegistry;
import org.jdbi.v3.core.statement.SqlLogger;
import org.jdbi.v3.core.statement.StatementContext;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ServiceContext {

    //TODO remove
    @Getter
    private final Jdbi jdbi;
    private final Map<Class<? extends DbService>, DbService> serviceMap;

    public ServiceContext(DataSource dataSource) {
        jdbi = initJdbi(dataSource);
        serviceMap = initServiceMap(jdbi);
    }

    @SuppressWarnings("unchecked")
    public <T extends DbService> T getService(Class<T> serviceClass) {
        return (T) serviceMap.get(serviceClass);
    }

    private Map<Class<? extends DbService>, DbService> initServiceMap(Jdbi jdbi) {
        //todo: adding all services

        @SuppressWarnings("unchecked")
        Map<Class<? extends DbService>, DbService> serviceMap = new HashMap();
        serviceMap.put(CustomerService.class, new CustomerService(jdbi));
        serviceMap.put(CustomerRoleService.class, new CustomerRoleService(jdbi));
        serviceMap.put(CurrencyService.class, new CurrencyService(jdbi));
        serviceMap.put(AccountService.class, new AccountService(jdbi));
        serviceMap.put(PhotoService.class, new PhotoService(jdbi));
        serviceMap.put(TransactionService.class, new TransactionService(jdbi));
        serviceMap.put(RateService.class, new RateService(jdbi));
        return Collections.unmodifiableMap(serviceMap);
    }

    private Jdbi initJdbi(DataSource dataSource) {
        return Jdbi.create(dataSource)
                .installPlugin(new SqlObjectPlugin())
                .setSqlLogger(new JdbiLogger())
                .registerArgument(new ServiceContext.BooleanArgumentFactory(Types.SMALLINT));
    }

    private static class BooleanArgumentFactory extends AbstractArgumentFactory<Boolean> {

        BooleanArgumentFactory(int sqlType) {
            super(sqlType);
        }

        @Override
        protected Argument build(Boolean value, ConfigRegistry config) {
            return (position, statement, ctx) -> statement.setInt(position, Boolean.TRUE.equals(value) ? 1 : 0);
        }
    }

    @Log4j2
    private static class JdbiLogger implements SqlLogger {
        @Override
        public void logBeforeExecution(StatementContext context) {
            if (!log.isTraceEnabled()) {
                log.debug("SQL statement prepared : {}", context.getRenderedSql());
                return;
            }

            try {
                log.trace("SQL statement prepared : {}", context.getStatement().unwrap(PreparedStatement.class));
            } catch (Exception e) {
                log.trace("SQL statement prepared : {}", context.getStatement());
            }
        }

    }
}
