package com.mede.jbank.entity.enums;

public enum Type {
    TRANSFER, CLAIM, DEPOSIT, WITHDRAW
}
