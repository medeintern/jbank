package com.mede.jbank.entity.enums;

public enum State {
    CREATED, STARTED, PENDING, CONFIRMED, DENIED, COMPLETED, FAILED
}
