package com.mede.jbank.entity.enums;

public enum Gender {
    MALE, FEMALE, UNDEFINED
}
