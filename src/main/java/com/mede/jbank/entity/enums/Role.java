package com.mede.jbank.entity.enums;

public enum Role {
    CUSTOMER, MANAGER
}
