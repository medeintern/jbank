package com.mede.jbank.entity;

import com.mede.jbank.entity.enums.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CustomerRole extends AbstractEntity {
    private Role role;
    private Long customerId;
}
