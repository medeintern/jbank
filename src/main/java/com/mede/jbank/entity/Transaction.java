package com.mede.jbank.entity;

import com.mede.jbank.entity.enums.State;
import com.mede.jbank.entity.enums.Type;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Transaction extends AbstractEntity {
    private Long senderAccountId;

    private Long recipientAccountId;

    private LocalDateTime transactionDate;

    @ColumnName("amount_funds")
    private BigDecimal amount;

    private Double rateSend;

    private Double rateRecipient;

    private String transactionNumber;

    private State state;

    private Type type;
}
