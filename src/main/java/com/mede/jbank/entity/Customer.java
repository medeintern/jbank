package com.mede.jbank.entity;

import com.mede.jbank.entity.enums.Gender;
import com.mede.jbank.validator.CustomerCheck;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "password", callSuper = true)
public class Customer extends AbstractEntity {

    @NotNull(message = "Name must not be null", groups = {CustomerCheck.class, Default.class})
    @Pattern(regexp = "[a-zA-Z\\d_\\-]{2,45}", groups = {CustomerCheck.class, Default.class}, message = "Name shouldn`t be shorter than 2 symbols and longer than 45." +
            " And must contain a-z A-Z letters, digits, - and _ symbols")
    private String name;

    @NotNull(message = "Surname must not be null", groups = {CustomerCheck.class, Default.class})
    @Pattern(regexp = "[a-zA-Z\\d_\\-]{2,45}", groups = {CustomerCheck.class, Default.class}, message = "Last Name shouldn`t be shorter than 2 symbols and longer than 45. " +
            "And must contain a-z A-Z letters, digits, - and _ symbols")
    private String surname;

    @NotNull(message = "Birthdate must not be null")
    @Past(message = "Birthday must be in past")
    private LocalDate birthdate;

    @NotNull(message = "Address must not be null", groups = {CustomerCheck.class, Default.class})
    @Pattern(regexp = "[a-zA-Z\\s\\d_\\-\\.,]{10,255}", groups = {CustomerCheck.class, Default.class}, message = "Address shouldn`t be shorter 10 symbols and longer than 255. " +
            "And must contain a-z A-Z letters, digits, and symbols -_.,")
    private String address;

    @NotNull(message = "Registration date must not be null")
    @PastOrPresent(message = "Registration date can not be in future")
    private LocalDateTime registrationDate;

    @NotNull(message = "Email must not be null", groups = {CustomerCheck.class, Default.class})
    @Size(max = 255, message = "Email shouldn`t have more than 255 symbols", groups = {CustomerCheck.class, Default.class})
    @Email(message = "Wrong email", groups = {CustomerCheck.class, Default.class})
    private String email;

    @NotNull(message = "Gender must not be null")
    private Gender gender;

    @NotNull(message = "Login must not be null")
    @Pattern(regexp = "^(?=.*[a-zA-Z])[a-zA-Z\\d\\._\\-]{5,25}$", message = "Login must have 5-25 symbols. At least one or more letters a-z, A-Z, digits and symbols - _ .")
    private String login;

    @NotNull(message = "Password must not be null")
    @Pattern(regexp = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})", message = "Password must have 8-40 symbols. " +
            "At least: one or more letters (a-z, A-Z), one or more digits, one or more special symbols(@#$%!), no whitespaces")
    private String password;

    @ColumnName("is_active")
    private Boolean isActive;

    @ColumnName("is_blocked")
    private Boolean isBlocked;

    @ColumnName("email_code")
    private UUID emailCode;
}
