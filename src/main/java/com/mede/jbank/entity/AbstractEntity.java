package com.mede.jbank.entity;

import lombok.Data;

@Data
abstract class AbstractEntity {
    private Long id;
}