package com.mede.jbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Rate extends AbstractEntity {
    @NotNull(message = "Currency id shouldn't be null")
    @Min(value = 0, message = "Currency id must be greater than 0")
    private Long currencyId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @NotNull(message = "Rate shouldn't be null")
    @Positive(message = "Rate shouldn't be 0 or negative")
    private Double rate;
}
