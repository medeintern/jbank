package com.mede.jbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Currency extends AbstractEntity {
    @NotNull(message = "Currency name must not be null")
    @Pattern(regexp = "[a-zA-Z\\d_\\t\\-]{2,20}", message = "Currency name shouldn`t be shorter than 2 symbols and longer than 20." +
            " And may contain a-z A-Z letters, digits, whitespace, - and _ symbols")
    private String name;
    @NotNull(message = "Currency abbreviation must not be null")
    @Pattern(regexp = "[A-Z]{3}", message = "Currency abbreviation should have 3 symbols length." +
            " And may contain only A-Z letters")
    private String abbreviation;
    private Boolean isBase;
    @NotNull(message = "Rate must not be null")
    @Positive(message = "Value must be larger than 0")
    private Double rate;
}
