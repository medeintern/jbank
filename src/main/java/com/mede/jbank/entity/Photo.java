package com.mede.jbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = "photo", callSuper = true)
public class Photo extends AbstractEntity {
    private Long customerId;
    private byte[] photo;
}
