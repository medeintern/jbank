package com.mede.jbank.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Account extends AbstractEntity {

    private String number;
    private Long customerId;
    private Long currencyId;
    private BigDecimal amount;
    private Boolean isBlocked;
}
