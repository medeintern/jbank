package com.mede.jbank.jwt;

import com.mede.jbank.entity.enums.Role;
import lombok.Data;

import java.util.List;

@Data
public class Principal {
    private Long id;
    private String name;
    private String login;
    private List<Role> roles;

}
