package com.mede.jbank.jwt;

import com.mede.jbank.entity.enums.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Log4j2
@UtilityClass
public class JwTokenHelper {
    public static final int TTL_MILLIS = 1800000;
    private static final String ISSUER = "JBank";
    private static final String LOGIN_CLAIM = "login";
    private static final String ID_CLAIM = "id";
    private static final String NAME_CLAIM = "name";
    private static final String ROLES_CLAIM = "roles";
    private static final Key KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static String createJwt(Principal principal, long ttl) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long expMillis = nowMillis + ttl;
        Date exp = new Date(expMillis);

        JwtBuilder builder = Jwts
                .builder()
                .setIssuer(ISSUER)
                .setIssuedAt(now)
                .claim(LOGIN_CLAIM, principal.getLogin())
                .claim(ID_CLAIM, principal.getId())
                .claim(NAME_CLAIM, principal.getName())
                .claim(ROLES_CLAIM, principal.getRoles())
                .setExpiration(exp)
                .signWith(KEY);

        return builder.compact();
    }

    public static String createJwt(Principal principal) {
        return createJwt(principal, TTL_MILLIS);
    }

    public static Principal parseJwt(String jwt) {
        Claims claims = Jwts.parser()
                .setSigningKey(KEY)
                .parseClaimsJws(jwt)
                .getBody();

        Principal principal = new Principal();
        principal.setLogin(claims.get(LOGIN_CLAIM, String.class));
        principal.setId(claims.get(ID_CLAIM, Long.class));
        principal.setName(claims.get(NAME_CLAIM, String.class));

        @SuppressWarnings("unchecked")
        List<String> list = (List<String>) claims.get(ROLES_CLAIM);
        List<Role> roleList = list.stream()
                .map(str -> Enum.valueOf(Role.class, str.toUpperCase(new Locale("UTF-8"))))
                .collect(Collectors.toList());

        principal.setRoles(roleList);

        return principal;
    }
}
