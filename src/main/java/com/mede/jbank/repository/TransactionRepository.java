package com.mede.jbank.repository;

import com.mede.jbank.dto.TransactionDetailDto;
import com.mede.jbank.dto.TransactionDto;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.State;

import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterFieldMapper(Transaction.class)
@RegisterFieldMapper(TransactionDto.class)
public interface TransactionRepository {

    @SqlUpdate("INSERT INTO transactions (sender_account_id, recipient_account_id, transaction_date, amount_funds, rate_send, rate_recipient, state, type) " +
            "VALUES (:senderAccountId, :recipientAccountId, :transactionDate, :amount, :rateSend, :rateRecipient, :state, :type)")
    @GetGeneratedKeys
    Transaction insert(@BindBean Transaction transaction);

    @SqlQuery("SELECT trans.id, trans.transaction_number, trans.amount_funds, curr.abbreviation, trans.type, trans.state, trans.transaction_date, acc1.number AS sender_number, " +
            "acc2.number AS recipient_number " +
            "FROM transactions AS trans " +
            "INNER JOIN accounts AS acc1 ON trans.sender_account_id = acc1.id " +
            "INNER JOIN accounts AS acc2 ON trans.recipient_account_id = acc2.id " +
            "INNER JOIN currencies AS curr ON acc1.currency_id = curr.id " +
            "WHERE acc1.customer_id = :id " +
            "OR acc2.customer_id = :id " +
            "ORDER BY trans.transaction_date DESC")
    List<TransactionDto> getListTransactionsByCustomerId(@Bind("id") Long id);

    @SqlQuery("SELECT t.id, t.transaction_number, t.amount_funds, t.type, t.state, t.transaction_date, a.number, cur.abbreviation, c.name, c.surname " +
            "FROM transactions AS t " +
            "INNER JOIN accounts AS a ON t.recipient_account_id = a.id " +
            "INNER JOIN customers AS c ON a.customer_id = c.id " +
            "INNER JOIN currencies AS cur ON a.currency_id = cur.id " +
            "WHERE a.customer_id = :accountId AND t.type = 'CLAIM' AND t.state = 'PENDING' " +
            "ORDER BY t.transaction_date DESC")
    List<TransactionDto> getPendingClaimsTransactions(@Bind("accountId") Long accountId);

    @SqlQuery("SELECT t.id, t.transaction_number, t.amount_funds, t.type, t.state, t.transaction_date, a.number, cur.abbreviation, c.name, c.surname " +
            "FROM transactions AS t " +
            "INNER JOIN accounts AS a ON t.sender_account_id = a.id " +
            "INNER JOIN customers AS c ON a.customer_id = c.id " +
            "INNER JOIN currencies AS cur ON a.currency_id = cur.id " +
            "WHERE a.customer_id = :accountId AND t.type = 'CLAIM' AND t.state = 'PENDING' " +
            "ORDER BY t.transaction_date DESC")
    List<TransactionDto> getIncomingClaimsTransactions(@Bind("accountId") Long accountId);

    @SqlUpdate("UPDATE transactions SET state = :state WHERE id =:id")
    boolean changeState(@Bind("id") Long id, @Bind("state") State state);

    @SqlQuery("SELECT * FROM transactions WHERE id = ?")
    Transaction findTransactionById(Long id);

    @RegisterFieldMapper(TransactionDetailDto.class)
    @SqlQuery("SELECT send_acc.number AS sender_number, sender.name AS sender_name, sender.surname AS sender_surname, " +
            "rec_acc.number AS recipient_number, recipient.name AS recipient_name, recipient.surname AS recipient_surname, " +
            "trans.amount_funds, curr.abbreviation " +
            "FROM transactions AS trans " +
            "INNER JOIN accounts AS send_acc ON trans.sender_account_id = send_acc.id " +
            "INNER JOIN accounts AS rec_acc ON trans.recipient_account_id = rec_acc.id " +
            "INNER JOIN customers AS sender ON send_acc.customer_id = sender.id " +
            "INNER JOIN customers AS recipient ON rec_acc.customer_id = recipient.id " +
            "INNER JOIN currencies AS curr ON send_acc.currency_id = curr.id " +
            "WHERE trans.id = ?")
    TransactionDetailDto findTransactionDetailDtoById(Long id);

    @SqlUpdate("UPDATE transactions SET transaction_number = :transactionNumber, sender_account_id = :senderAccountId, recipient_account_id = :recipientAccountId, " +
            "transaction_date = :transactionDate, amount_funds = :amount, rate_send = :rateSend, rate_recipient = :rateRecipient, state = :state, type= :type " +
            "WHERE id = :id")
    boolean update(@BindBean Transaction transaction);

    @SqlQuery("SELECT * FROM transactions WHERE sender_account_id = :accountId OR recipient_account_id = :accountId ")
    List<Transaction> getTransactionListByAccountId(@Bind("accountId") Long accountId);
}