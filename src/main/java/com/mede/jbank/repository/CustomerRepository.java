package com.mede.jbank.repository;

import com.mede.jbank.entity.Customer;

import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;
import java.util.UUID;

@RegisterFieldMapper(Customer.class)
public interface CustomerRepository {

    @SqlQuery("SELECT * FROM customers WHERE login=:login")
    Customer findByLogin(@Bind("login") String login);

    @SqlQuery("SELECT * FROM customers WHERE id=:id")
    Customer findById(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM customers WHERE email = :email")
    Customer findByEmail(@Bind("email") String email);

    @SqlQuery("SELECT * FROM customers")
    List<Customer> getListCustomers();

    @SqlQuery("SELECT EXISTS(SELECT id FROM customers WHERE login = :login OR email = :email)")
    boolean isExistCustomerByLoginOrEmail(@BindBean Customer customer);

    @SqlUpdate("INSERT INTO customers (name, surname, birthdate, address, registration_date, email, gender, login, password, is_active, is_blocked) " +
            "VALUES (:name, :surname, :birthdate, :address, :registrationDate, :email, :gender, :login, :password, :isActive, :isBlocked)")
    @GetGeneratedKeys
    Customer insert(@BindBean Customer customer);

    @SqlUpdate("UPDATE customers SET name = :name,  surname = :surname, address = :address, email = :email, password = :password " +
            "WHERE id = :id")
    boolean updateCustomer(@BindBean Customer customer);

    @SqlUpdate("UPDATE customers SET is_active = 1 WHERE email_code = :code AND is_active = 0")
    boolean activateByCode(@Bind("code") UUID code);

    @SqlUpdate("UPDATE customers SET is_blocked = :isBlocked WHERE id = :id")
    boolean changeBlocked(@Bind("id") Long id, @Bind("isBlocked") boolean isBlocked);

    @SqlUpdate("UPDATE customers SET email_code=null WHERE email_code = :code")
    boolean deleteCode(@Bind("code") UUID code);
}
