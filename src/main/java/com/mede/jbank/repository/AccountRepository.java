package com.mede.jbank.repository;

import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.dto.BaseAccountDto;
import com.mede.jbank.entity.Account;

import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.math.BigDecimal;
import java.util.List;

@RegisterFieldMapper(Account.class)
public interface AccountRepository {

    @RegisterFieldMapper(AccountDto.class)
    @SqlQuery("SELECT accounts.id, accounts.number, customers.login, customers.name, customers.surname, currencies.abbreviation, currencies.is_base, accounts.amount, " +
            "accounts.is_blocked " +
            "FROM accounts " +
            "INNER JOIN customers ON accounts.customer_id = customers.id " +
            "INNER JOIN currencies ON accounts.currency_id = currencies.id " +
            "WHERE customers.login LIKE '%' || :login || '%' " +
            "AND LOWER(customers.name) LIKE '%' || :name || '%' " +
            "AND LOWER(customers.surname) LIKE '%' || :surname || '%'")
    List<AccountDto> searchAccountByCustomerFields(@Bind("login") String login, @Bind("name") String name, @Bind("surname") String surname);

    @RegisterFieldMapper(AccountDto.class)
    @SqlQuery("SELECT accounts.id, accounts.number, customers.login, customers.name, customers.surname, currencies.abbreviation, currencies.is_base, accounts.amount, " +
            "accounts.is_blocked " +
            "FROM accounts " +
            "INNER JOIN customers ON accounts.customer_id = customers.id " +
            "INNER JOIN currencies ON accounts.currency_id = currencies.id " +
            "WHERE accounts.number = :number")
    AccountDto findAccountDtoByNumber(@Bind("number") String number);

    @SqlQuery("SELECT * FROM accounts WHERE id = ?")
    Account findAccountById(Long id);

    @SqlQuery("SELECT id FROM accounts WHERE number = ? AND is_blocked = 0")
    Long findWorkingAccountByNumber(String number);

    @SqlQuery("SELECT * FROM accounts WHERE number = ?")
    Account findAccountByNumber(String number);

    @SqlQuery("SELECT id FROM accounts WHERE number = ?")
    Long findAccountIdByNumber(String number);

    @RegisterFieldMapper(AccountDto.class)
    @SqlQuery("SELECT accounts.id, accounts.number, customers.login, customers.name, customers.surname, currencies.abbreviation, currencies.is_base, accounts.amount, " +
            "accounts.is_blocked " +
            "FROM accounts " +
            "INNER JOIN customers ON accounts.customer_id = customers.id " +
            "INNER JOIN currencies ON accounts.currency_id = currencies.id " +
            "WHERE customers.id = ?")
    List<AccountDto> searchAccountsByCustomerId(Long id);

    @SqlUpdate("UPDATE accounts SET is_blocked = :isBlocked WHERE id = :id")
    boolean changeBlocked(@Bind("id") Long id, @Bind("isBlocked") boolean isBlocked);

    @RegisterFieldMapper(BaseAccountDto.class)
    @SqlQuery("SELECT accounts.id, accounts.number, currencies.abbreviation, accounts.amount, accounts.is_blocked " +
            "FROM accounts " +
            "INNER JOIN currencies ON accounts.currency_id = currencies.id " +
            "WHERE accounts.customer_id = ? AND currencies.is_base = 1")
    BaseAccountDto findBaseAccountDtoByCustomerId(Long id);

    @SqlQuery("SELECT accounts.number " +
            "FROM accounts " +
            "INNER JOIN currencies ON accounts.currency_id = currencies.id " +
            "WHERE accounts.customer_id = 1 AND currencies.is_base = 1")
    String findAdminBaseAccountNumber();

    @SqlQuery("SELECT currencies.is_base " +
            "FROM currencies " +
            "INNER JOIN accounts ON accounts.currency_id = currencies.id " +
            "WHERE accounts.number = ?")
    boolean isBaseAccount(String number);

    @SqlQuery("SELECT amount FROM accounts WHERE id = ?")
    BigDecimal getAmountByAccountId(Long id);

    @SqlQuery("SELECT currencies.rate FROM currencies " +
            "INNER JOIN accounts ON accounts.currency_id = currencies.id " +
            "WHERE accounts.id = ?")
    Double getRateByAccountId(Long id);

    @SqlUpdate("UPDATE accounts SET amount = amount + :transferSum WHERE id = :id")
    boolean updateAmountAccount(@Bind("id") Long id, @Bind("transferSum") BigDecimal transferSum);

    @RegisterFieldMapper(Account.class)
    @SqlUpdate("INSERT INTO accounts (customer_id, currency_id, amount, is_blocked) " +
            "VALUES (:customerId, :currencyId, :amount, :isBlocked)")
    @GetGeneratedKeys
    Account saveAccount(@BindBean Account account);

    @RegisterFieldMapper(Account.class)
    @SqlQuery("SELECT COUNT(currency_id) AS count FROM accounts WHERE customer_id = :customerId AND currency_id = :currencyId")
    Long findNumberCurrenciesByCustomer(@Bind("customerId") Long customerId, @Bind("currencyId") Long currencyId);
}
