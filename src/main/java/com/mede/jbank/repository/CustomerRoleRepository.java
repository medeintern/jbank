package com.mede.jbank.repository;

import com.mede.jbank.entity.CustomerRole;
import com.mede.jbank.entity.enums.Role;
import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterFieldMapper(CustomerRole.class)
public interface CustomerRoleRepository {

    @SqlQuery("SELECT role FROM roles WHERE customer_id=:id")
    List<CustomerRole> findRolesById(@Bind("id") Long id);

    @SqlUpdate("INSERT INTO roles (role, customer_id) VALUES (:role, :id)")
    boolean insert(@Bind("id") Long id, @Bind("role") Role role);
}
