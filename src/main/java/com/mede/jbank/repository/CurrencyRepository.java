package com.mede.jbank.repository;

import com.mede.jbank.entity.Currency;
import org.jdbi.v3.sqlobject.config.KeyColumn;
import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.config.ValueColumn;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;
import java.util.Map;

@RegisterFieldMapper(Currency.class)
public interface CurrencyRepository {

    @SqlQuery("SELECT abbreviation, rate FROM currencies")
    @KeyColumn("abbreviation")
    @ValueColumn("rate")
    Map<String, Double> findMapCurrenciesWithRates();

    @SqlQuery("SELECT * FROM currencies WHERE is_base = 1")
    Currency findBaseCurrency();

    @SqlQuery("SELECT rate FROM currencies WHERE  id = ?")
    Double findRateByCurrencyId(Long currencyId);

    @SqlQuery("SELECT * FROM currencies")
    List<Currency> findAllCurrencies();

    @SqlQuery("SELECT * FROM currencies WHERE name = :name")
    Currency findCurrencyByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM currencies WHERE id = :id")
    Currency findCurrencyById(@Bind("id") Long id);

    @SqlQuery("SELECT name, COUNT(accounts.currency_id) < :maxAccountNumber AS count FROM currencies LEFT JOIN accounts " +
            "ON currencies.id = accounts.currency_id AND accounts.customer_id = :customerId " +
            "WHERE currencies.is_base != 1 GROUP BY name")
    @KeyColumn("name")
    @ValueColumn("count")
    Map<String, Boolean> findAvailableCurrenciesForNewAccount(@Bind("customerId") Long customerId, @Bind("maxAccountNumber") Long maxAccountNumber);

    @SqlUpdate("INSERT INTO currencies (name, abbreviation, is_base, rate) " +
            "VALUES (:name, :abbreviation, :isBase, :rate)")
    @GetGeneratedKeys
    Currency insert(@BindBean Currency currency);

    @SqlUpdate("UPDATE currencies SET rate = :rate WHERE id = :id")
    boolean updateCurrencyRate(@Bind("id") Long id, @Bind("rate") Double rate);
}
