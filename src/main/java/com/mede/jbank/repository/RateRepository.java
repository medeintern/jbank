package com.mede.jbank.repository;

import com.mede.jbank.entity.Rate;
import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

@RegisterFieldMapper(Rate.class)
public interface RateRepository {
    @SqlQuery("SELECT * FROM rates")
    List<Rate> findAllRates();

    @SqlUpdate("INSERT INTO rates (currency_id, start_date, end_date, rate) " +
            "VALUES (:currencyId, :startDate, :endDate, :rate)")
    @GetGeneratedKeys
    Rate insert(@BindBean Rate rate);

    @SqlUpdate("UPDATE rates SET end_date = :startDate WHERE currency_id = :currencyId AND end_date IS NULL")
    @GetGeneratedKeys
    Rate updateLastRate(@BindBean Rate rate);
}
