package com.mede.jbank.repository;

import com.mede.jbank.entity.Photo;
import org.jdbi.v3.sqlobject.config.RegisterFieldMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

@RegisterFieldMapper(Photo.class)
public interface PhotoRepository {

    @SqlQuery("SELECT * FROM photos WHERE customer_id = :customerId")
    Photo findByCustomerId(@Bind("customerId") Long customerId);

    @SqlUpdate("INSERT INTO photos (photo, customer_id) " +
            "VALUES (:photo, :customerId)")
    @GetGeneratedKeys
    Photo insert(@BindBean Photo photo);

    @SqlUpdate("UPDATE photos SET photo = :photo WHERE customer_id = :customerId")
    boolean updatePhoto(@BindBean Photo photo);
}
