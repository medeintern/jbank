package com.mede.jbank.email;

import com.mede.jbank.entity.Customer;

import lombok.extern.log4j.Log4j2;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Log4j2
public class EmailSender {

    private static final String FILEPATH_TO_PROP = "src/main/resources/mailtrap.PROPERTIES";
    private static final Properties PROPERTIES = new Properties();
    private static final String USERNAME;
    private static final String PASSWORD;
    private static final String EMAIL;
    private static final String HOST;
    private static final String PORT;
    private static final String SUBJECT;
    private static final String TYPE;

    static {
        try (FileInputStream fis = new FileInputStream(FILEPATH_TO_PROP)) {
            PROPERTIES.load(fis);
        } catch (IOException e) {
            log.error("Can`t read PROPERTIES.");
        }
        USERNAME = PROPERTIES.getProperty("mailtrap.username");
        PASSWORD = PROPERTIES.getProperty("mailtrap.password");
        EMAIL = PROPERTIES.getProperty("mailtrap.email");
        HOST = PROPERTIES.getProperty("mailtrap.host");
        PORT = PROPERTIES.getProperty("mailtrap.port");
        SUBJECT = PROPERTIES.getProperty("mailtrap.subject");
        TYPE = PROPERTIES.getProperty("mailtrap.type");
    }

    public static void sendEmail(Customer customer) {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", HOST);
            props.put("mail.smtp.port", PORT);

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(
                                    USERNAME,
                                    PASSWORD);
                        }
                    });
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(EMAIL));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(customer.getEmail()));

            message.setSubject(SUBJECT);

            String code = customer.getEmailCode().toString();
            String text = "<h1>Click on the link below to activate your account.</h1>\n" +
                    "<a href=\"http://localhost:8080/emailController?code=" + code + "\">ClickHere</a>";
            message.setContent(text, TYPE);
            Transport.send(message);

        } catch (MessagingException e) {
            log.error("Message to customer with id = {} was not sent", customer.getId());
        }
    }
}


