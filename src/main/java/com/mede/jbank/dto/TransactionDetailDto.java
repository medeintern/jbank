package com.mede.jbank.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class TransactionDetailDto {

    @ColumnName("sender_number")
    private String senderAccountNumber;

    @ColumnName("recipient_number")
    private String recipientAccountNumber;

    @ColumnName("sender_name")
    private String senderName;

    @ColumnName("sender_surname")
    private String senderSurname;

    @ColumnName("recipient_name")
    private String recipientName;

    @ColumnName("recipient_surname")
    private String recipientSurname;

    @ColumnName("amount_funds")
    private BigDecimal amount;

    @ColumnName("abbreviation")
    private String currencyAbbreviation;
}
