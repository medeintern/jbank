package com.mede.jbank.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class AccountDto {

    @ColumnName("id")
    private Long accountId;

    @ColumnName("number")
    private String accountNumber;

    @ColumnName("login")
    private String customerLogin;

    @ColumnName("name")
    private String customerName;

    @ColumnName("surname")
    private String customerSurname;

    @ColumnName("abbreviation")
    private String currencyAbbreviation;

    @ColumnName("is_base")
    private Boolean isBase;

    @ColumnName("amount")
    private BigDecimal balance;

    @ColumnName("is_blocked")
    private Boolean isBlocked;
}
