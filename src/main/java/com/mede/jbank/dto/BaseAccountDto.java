package com.mede.jbank.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class BaseAccountDto {

    @ColumnName("id")
    private Long accountId;

    @ColumnName("number")
    private String accountNumber;

    @ColumnName("amount")
    private BigDecimal balance;

    @ColumnName("abbreviation")
    private String currencyAbbreviation;

    @ColumnName("is_blocked")
    private Boolean isBlocked;
}
