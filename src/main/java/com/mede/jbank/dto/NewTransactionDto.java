package com.mede.jbank.dto;

import com.mede.jbank.entity.enums.Type;
import lombok.Builder;
import lombok.Data;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;

@Data
@Builder
public class NewTransactionDto {

    @ColumnName("sender_number")
    private String senderAccountNumber;

    @ColumnName("recipient_number")
    private String recipientAccountNumber;

    @ColumnName("amount_funds")
    private BigDecimal amount;

    private Type type;
}
