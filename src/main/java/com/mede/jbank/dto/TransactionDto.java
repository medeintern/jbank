package com.mede.jbank.dto;

import com.mede.jbank.entity.enums.State;
import com.mede.jbank.entity.enums.Type;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.mapper.reflect.ColumnName;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class TransactionDto {

    @ColumnName("id")
    private Long transactionId;

    private String transactionNumber;

    @ColumnName("name")
    private String customerName;

    @ColumnName("surname")
    private String customerSurname;

    @ColumnName("amount_funds")
    private BigDecimal amount;

    @ColumnName("abbreviation")
    private String currencyAbbreviation;

    private Type type;

    private State state;

    private LocalDateTime transactionDate;

    @ColumnName("sender_number")
    private String senderAccountNumber;

    @ColumnName("recipient_number")
    private String recipientAccountNumber;

}
