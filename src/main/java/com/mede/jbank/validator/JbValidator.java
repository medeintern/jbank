package com.mede.jbank.validator;

import lombok.AllArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
//@TraceLogger
public class JbValidator {

    private Validator validator;

    public <T> Set<ConstraintViolation<T>> validate(T objectForValidation, Class<?>... groupField) {
        return validator.validate(objectForValidation, groupField);
    }

    public <T> Set<ConstraintViolation<T>> validateProperty(T objectForValidation, String fieldName, Class<?>... groupField) {
        return validator.validateProperty(objectForValidation, fieldName, groupField);
    }

    public <T> Set<ConstraintViolation<T>> validateValue(Class<T> beanType, String fieldName, Object value, Class<?>... groups) {
        return validator.validateValue(beanType, fieldName, value, groups);
    }

    public Set<String> validateOutSetString(Object objectForValidation, Class<?>... groupField) {
        return validator.validate(objectForValidation, groupField).stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
    }

    public Set<String> validatePropertyOutSetString(Object objectForValidation, String fieldName, Class<?>... groupField) {
        return validator.validateProperty(objectForValidation, fieldName, groupField).stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
    }

    public Set<String> validateValueOutSetString(Class<?> beanType, String fieldName, Object value, Class<?>... groups) {
        return validator.validateValue(beanType, fieldName, value, groups).stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
    }
}
