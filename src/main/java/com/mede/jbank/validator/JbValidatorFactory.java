package com.mede.jbank.validator;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public enum JbValidatorFactory {

    INSTANCE;

    private final transient ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

    public JbValidator getValidator() {
        Validator validator = validatorFactory.getValidator();
        return new JbValidator(validator);
    }

}

