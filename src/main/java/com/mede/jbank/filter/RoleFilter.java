package com.mede.jbank.filter;

import com.mede.jbank.annotation.AccessRole;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.exception.RoleAccessException;
import com.mede.jbank.exception.UnauthorizedException;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.util.WebUtils;
import lombok.extern.log4j.Log4j2;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;


@Log4j2
@WebFilter(urlPatterns = "/secured/*")
public class RoleFilter extends AbstractFilter {
    private Map<String, List<Role>> servlets;

    @Override
    public void init(FilterConfig filterConfig) {
        servlets = new HashMap<>();

        Map<String, ? extends ServletRegistration> servletRegistrations = filterConfig.getServletContext().getServletRegistrations();

        for (Map.Entry<String, ? extends ServletRegistration> entry : servletRegistrations.entrySet()) {
            ServletRegistration servletRegistration = entry.getValue();
            Collection<String> mappings = servletRegistration.getMappings();


            for (String servletPath : mappings) {
                if (servletPath.endsWith("/*")) {
                    servletPath = servletPath.substring(0, servletPath.lastIndexOf("/"));
                }
                List<Role> listRolesForServlet = findAccessRolesForServlet(servletRegistration.getClassName());
                if (listRolesForServlet != null && !listRolesForServlet.isEmpty()) {
                    servlets.put(servletPath, listRolesForServlet);
                }
            }
        }
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        Principal principal = (Principal) request.getAttribute(WebUtils.USER_PRINCIPAL);
        String servletPath = request.getServletPath();

        if (principal != null) {

            List<Role> userRoles = principal.getRoles();

            if (checkRole(userRoles, servletPath)) {
                chain.doFilter(request, response);
            } else {
                log.warn("Access to servletPath \"{}\" denied for user {}.", servletPath, principal.getLogin());
                throw new RoleAccessException();
            }
        } else {
            log.warn("Customer should log in to rich servletPath \"{}\".", servletPath);
            throw new UnauthorizedException();
        }
    }

    private boolean checkRole(List<Role> userRoles, String servletPath) {

        List<Role> rolesForServlet = servlets.get(servletPath);
        if (rolesForServlet != null) {
            if (!rolesForServlet.isEmpty()) {

                for (Role userRole : userRoles) {
                    if (rolesForServlet.contains(userRole)) {
                        return true;
                    }
                }
            }
        } else {
            log.warn("Can`t reach servlet with servletPath \"{}\".", servletPath);
        }
        return false;
    }

    private List<Role> findAccessRolesForServlet(String servletName) {

        try {
            Class<?> servlet = Class.forName(servletName);
            AccessRole annotation = servlet.getAnnotation(AccessRole.class);

            if (annotation != null) {
                return Arrays.asList(annotation.roles());
            } else {
                log.warn("Servlet {} does not have RoleAnnotation", servletName);
                return null;
            }
        } catch (ClassNotFoundException e) {
            log.error("Servlet {} does not exist.", servletName);
            throw new RuntimeException(e);
        }

    }

}
