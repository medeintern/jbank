package com.mede.jbank.filter;

import lombok.extern.log4j.Log4j2;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
public abstract class AbstractFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            req.setCharacterEncoding("UTF-8");
            doFilter(req, resp, chain);
        } else {
            log.error("Type casting error ServletRequest to HttpServletRequest");
            throw new ServletException("Type casting error");
        }
    }

    public abstract void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException;

    @Override
    public void destroy() {
    }
}
