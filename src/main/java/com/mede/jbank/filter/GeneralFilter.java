package com.mede.jbank.filter;

import com.mede.jbank.jwt.JwTokenHelper;
import com.mede.jbank.jwt.Principal;
import com.mede.jbank.util.WebUtils;
import lombok.extern.log4j.Log4j2;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Log4j2
@WebFilter(urlPatterns = "/*")
public class GeneralFilter extends AbstractFilter {

    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        Cookie userTokenCookie = WebUtils.findCookie(req, WebUtils.USER_TOKEN_COOKIE_NAME);

        if (Objects.nonNull(userTokenCookie)) {
            try {
                Principal principal = JwTokenHelper.parseJwt(userTokenCookie.getValue());
                req.setAttribute(WebUtils.USER_PRINCIPAL, principal);
            } catch (Exception e) {
                log.warn("Can`t trust this JWT.");
                WebUtils.deleteCookie(resp, userTokenCookie);
                req.setAttribute(WebUtils.USER_PRINCIPAL, null);
            }
        }
        chain.doFilter(req, resp);
    }
}

