package com.mede.jbank.exception;

import java.util.Set;

public class ValidationException extends RuntimeException {

    Set<String> validationErrors;

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(Set<String> set) {
        this.validationErrors = set;
    }

    public ValidationException(Set<String> set, String message) {
        super(message);
        this.validationErrors = set;
    }

    public ValidationException(String message, Throwable err) {
        super(message, err);
    }

    public ValidationException(Set<String> set, String message, Throwable err) {
        super(message, err);
        this.validationErrors = set;
    }

}
