package com.mede.jbank.exception;

public class RoleAccessException extends RuntimeException {
    public RoleAccessException() {
    }

    public RoleAccessException(String message) {
        super(message);
    }
}
