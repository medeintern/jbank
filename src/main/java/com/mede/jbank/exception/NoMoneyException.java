package com.mede.jbank.exception;

public class NoMoneyException extends Exception {

    public NoMoneyException(String message) {
        super(message);
    }
}


