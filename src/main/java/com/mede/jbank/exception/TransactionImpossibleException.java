package com.mede.jbank.exception;

public class TransactionImpossibleException extends RuntimeException {
    public TransactionImpossibleException() {
        super();
    }

    public TransactionImpossibleException(String msg) {
        super(msg);
    }
}
