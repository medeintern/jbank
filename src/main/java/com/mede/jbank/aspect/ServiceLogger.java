package com.mede.jbank.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public final class ServiceLogger {

    @Pointcut("execution(public * com.mede.jbank.service.impl.*.*(..))")
    private void serviceMethodCall() {
    }

    @Pointcut("@within(com.mede.jbank.annotation.TraceLogger)")
    private void annotatedClass() {
    }

    @Pointcut("execution(public * *(..))")
    private void publicMethod() {
    }

    @Around("serviceMethodCall() || (annotatedClass() && publicMethod())")
    public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
        Logger log = LogManager.getLogger(pjp.getTarget().getClass());
        if (log.isTraceEnabled()) {
            log.trace("Service method called. {}.{}(..) Args: {}", pjp.getTarget().getClass().getName(),
                    pjp.getSignature().getName(), pjp.getArgs());
        }

        Object result = pjp.proceed();

        if (log.isTraceEnabled()) {
            log.trace("Service method returned. {}.{}. Result: {}", pjp.getTarget().getClass().getName(),
                    pjp.getSignature().getName(), result);
        }

        return result;
    }
}
