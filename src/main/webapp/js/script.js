function convertDateFromUTC(datatime) {
    var utcDate = new Date(datatime);
    var newDate = utcDate.toString().replace(/GMT.*/g, "").replace(/T/g, " ");
    var localDate = new Date(newDate + " UTC");
    return localDate.toString()
        .replace(/GMT.*/g, "")
        .substring(4);
}