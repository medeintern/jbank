<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Transactions > Transactions List </h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/transactionsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <table id="transactionsTable" class="table table-striped table-bordered text-center"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Transaction ID</th>
                            <th>Amount</th>
                            <th>Currency</th>
                            <th>Type</th>
                            <th>State</th>
                            <th>Timestamp</th>
                            <th>Sender Account</th>
                            <th>Recipient Account</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="transaction" items="${transactionDtoList}" varStatus="count">
                            <tr>
                                <th><c:out value="${count.count}"/></th>
                                <td><c:out value="${transaction.transactionNumber}"/></td>
                                <td><c:out value="${transaction.amount}"/></td>
                                <td><c:out value="${transaction.currencyAbbreviation}"/></td>
                                <td><c:out value="${transaction.type}"/></td>
                                <td><c:out value="${transaction.state}"/></td>
                                <td class="date-td"><c:out value="${transaction.transactionDate}"/></td>
                                <td><c:out value="${transaction.senderAccountNumber}"/></td>
                                <td><c:out value="${transaction.recipientAccountNumber}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th class="hidden-th"></th>
                            <th class="hidden-th"></th>
                            <th class="hidden-th"></th>
                            <th>Currency</th>
                            <th>Type</th>
                            <th>State</th>
                            <th class="hidden-th"></th>
                            <th class="hidden-th"></th>
                            <th class="hidden-th"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(document).ready(function () {
        $('#transactionsTable').DataTable({
            "scrollX": true,
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    if (!jQuery(column.footer()).hasClass('hidden-th')) {
                        var select = $('<select><option value="">ALL</option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            }
        });

        $('.date-td').each(function () {
            this.innerHTML = convertDateFromUTC(this.innerHTML);
        });
    });
</script>
</body>
</html>