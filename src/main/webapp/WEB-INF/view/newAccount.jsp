<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Accounts > New Account</h4>
            <hr>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/accountsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <h2>Create new account</h2>
                    <c:choose>
                        <c:when test="${Message != null}">
                            <h5 style="color:red;">
                                <c:out value="${Message}"/>
                            </h5>
                        </c:when>
                    </c:choose>
                    <form action="/secured/newAccount" method="post">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Account currency </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="accountCurrency">
                                    <c:forEach var="currency" items="${currencyMap}">
                                        <c:choose>
                                            <c:when test="${currency.getValue()}">
                                                <option>
                                                    <c:out value="${currency.getKey()} "/>
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option disabled style="color: #f00">
                                                    <c:out value="${currency.getKey()}"/>
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-group-sm btn-primary">Create account</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

</body>
</html>