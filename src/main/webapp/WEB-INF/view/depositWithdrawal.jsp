<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Transactions > Transaction List</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/transactionsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <%@ include file="/WEB-INF/jspf/depositWithdrawal.jspf" %>
                </div>
            </div>
        </div>
    </div>


</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>