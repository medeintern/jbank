<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<c:set var="customer" value="${customer}"/>

<div class="container registerContainer">
    <form action="${pageContext.request.contextPath}/registration" method="post">
        <h2 class="text-left">Register Customer</h2>

        <div class="col-sm-10">
            <hr>
        </div>

        <c:if test="${failCustomerSet !=null}">
            <div class="alert alert-danger col-sm-10" role="alert">
                <c:forEach var="failSet" items="${failCustomerSet}">
                    <c:out value="${failSet}"/><br>
                </c:forEach>
            </div>
        </c:if>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Login</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="login" placeholder="Login" value="${customer.login}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Retype Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" name="retypePassword" placeholder="Retype Password"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="email" placeholder="Email" value="${customer.email}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">FirstName</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="firstName" placeholder="First Name"
                       value="${customer.name}" required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Last Name</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="lastName" placeholder="Last Name"
                       value="${customer.surname}" required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Gender</label>
            <div class="col-sm-8">
                <select class="form-control" name="gender">
                    <c:forEach var="gender" items="${genderList}">
                        <option>
                            <c:out value="${gender}"/>
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Address</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="address" placeholder="Address" value="${customer.address}"
                       required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Birthday</label>
            <div class="col-8">
                <input class="form-control" type="date" placeholder="mm/dd/yyyy" value="${customer.birthdate}"
                       name="birthday" required>
            </div>
        </div>

        <button type="submit" class="btn btn-group-sm btn-primary">Submit</button>
    </form>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>

</html>