<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isErrorPage="true" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="form">
    <h2> Error! </h2>
    <div>
        <b>Status code:</b> ${status.status}
    </div>
    <div>
        <b>URI :</b> ${pageContext.errorData.requestURI}
    </div>
    <div>
        <b>Message:</b> ${status.message}
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>
