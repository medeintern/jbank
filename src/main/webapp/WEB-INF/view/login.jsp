<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container h-50">
    <div class="row h-100 justify-content-center align-items-center">
        <form action="${pageContext.request.contextPath}/login" method="post" class="col-6">
            <div class="card">
                <article class="card-body">
                    <h4 class="card-title text-center mb-4 mt-1">jBank Login</h4>
                    <hr>
                    <c:if test="${messageError != null}">
                        <div class="alert alert-danger" role="alert">
                            <b><c:out value="${messageError}"/></b>
                        </div>
                    </c:if>
                    <form>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                </div>
                                <input name="login" class="form-control" placeholder="Login" type="text"
                                       value="${inputLogin}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                </div>
                                <input class="form-control" name="password" placeholder="Password" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="signup" class="btn btn-primary btn-block"
                                    style="background-color: #0D5B9D">
                                Login
                            </button>
                        </div>
                        <p>Forgot password? Please <a href="">contact us!</a></p>
                        <p>Don't have account? Please <a href="${pageContext.request.contextPath}/registration">register!</a>
                        </p>
                    </form>
                </article>
            </div>
        </form>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>