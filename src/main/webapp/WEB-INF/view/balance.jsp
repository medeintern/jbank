<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Accounts > Balance</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/accountsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <%@ include file="/WEB-INF/jspf/balance.jspf" %>
                </div>
            </div>
        </div>
    </div>


</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(document).ready(function () {
        $('#balanceTable').DataTable();
    });
</script>
</body>
</html>