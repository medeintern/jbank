<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Admin > Change rate</h4>
            <hr>
        </div>
        <c:if test="${errors != null}">
            <div class="alert alert-danger col-sm-10" role="alert">
                <c:forEach var="error" items="${errors}">
                    <ul>
                        <li><c:out value="${error}"/></li>
                    </ul>
                </c:forEach>
            </div>
        </c:if>
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/adminSideBar.jspf" %>
                </div>
                <div class="col-6">
                    <table id="balanceTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Currency code</th>
                            <th scope="col">Currency name</th>
                            <th scope="col">JCN per Unit</th>
                            <th scope="col">Change Rate</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="currency" items="${currencies}" varStatus="count">
                            <tr>
                                <th scope="row"><c:out value="${count.count}"/></th>
                                <td><c:out value="${currency.abbreviation}"/></td>
                                <td><c:out value="${currency.name}"/></td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="4"
                                                      value="${currency.rate}"/></td>
                                <c:if test="${! currency.isBase}">
                                    <td>
                                        <form name="changeRateForm" data-toggle="modal" data-target="#changeRateModal">
                                            <button type="button" id="changeBtn" class="btn btn-primary"
                                                    data-toggle="modal" data-target="#changeRateModal"
                                                    data-name="${currency.name}"  data-current_rate="${currency.rate}" data-id="${currency.id}">
                                                Change
                                            </button>
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="changeRateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <form action="${pageContext.request.contextPath}/secured/admin/newRate" method="post">
                    <input type="number" step="0.0001" min="0" id="rate" name="rate" placeholder="1.00">
                    <input type="hidden" name="id" id="id">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" style="background-color: #0D5B9D">Change</button>
                </form>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
<script>
    $('#changeRateModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('name')
        var rate = button.data('current_rate')
        var id = button.data('id')
        var modal = $(this)
        modal.find('.modal-title').text('Change rate ' + recipient + '. Current rate: ' + rate)
        modal.find('#rate').val(rate)
        modal.find('#id').val(id)
    })
</script>
<script>
    $(document).ready(function () {
        $('#balanceTable').DataTable();
    });
</script>
</body>
</html>