<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">

    <div class="card">

        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Admin > Add New Currency</h4>
            <hr>
        </div>

        <div class="card-body">
            <c:if test="${errors != null}">
                <div class="alert alert-danger col-sm-10" role="alert">
                    <c:forEach var="error" items="${errors}">
                        <ul>
                            <li><c:out value="${error}"/></li>
                        </ul>
                    </c:forEach>
                </div>
            </c:if>

            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/adminSideBar.jspf" %>
                </div>

                <div class="col-6">
                    <form action="${pageContext.request.contextPath}/secured/admin/newCurrency" method="post">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Currency Name</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" name="name" value="${currency.name}"
                                       placeholder="Enter currency name" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Abbreviation</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="abbreviation"
                                       value="${currency.abbreviation}"
                                       placeholder="Enter abbreviation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Currency rate</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="number" step="0.0001" min="0" name="rate"
                                       value="${currency.rate}"
                                       placeholder="1.00" required>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-group-sm btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>

    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>

</html>