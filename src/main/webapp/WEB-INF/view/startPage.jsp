<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-center mb-4 mt-1" style="font-size:50px;">Welcome to jBank!</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-9">
                    <ul class="list-unstyled">
                        <li class="media">
                            <div class="align-self-center  mr-3">
                                <i class="fa fa-3x fa-university" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">Reliable Bank</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                sollicitudin.
                                Cras
                                purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                                nisi
                                vulputate fringilla. Donec lacinia congue felis in faucibus.
                                <p/>
                            </div>
                        </li>
                        <li class="media">
                            <div class="align-self-center  mr-3">
                                <i class="fa fa-3x fa-money" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">Secure Transactions</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                sollicitudin.
                                Cras
                                purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                                nisi
                                vulputate fringilla. Donec lacinia congue felis in faucibus.
                                <p/>
                            </div>
                        </li>

                        <li class="media">
                            <div class="align-self-center  mr-3">
                                <i class="fa fa-3x fa-retweet" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">Currency Exchange</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                sollicitudin.
                                Cras
                                purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                                nisi
                                vulputate fringilla. Donec lacinia congue felis in faucibus.
                                <p/>
                            </div>
                        </li>

                        <li class="media">
                            <div class="align-self-center  mr-3">
                                <i class="fa fa-3x fa-gift" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">Claim Requests</h5>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                sollicitudin.
                                Cras
                                purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                                nisi
                                vulputate fringilla. Donec lacinia congue felis in faucibus.
                                <p/>
                            </div>
                        </li>

                    </ul>
                </div>

                <div class="col-3 border">
                    <h5 class="mt-0 mb-1">Exchange rates:</h5>
                    <c:if test="${currencyList !=null}">
                        <c:forEach items="${currencyList}" var="currency">
                            <c:if test="${currency.key ne requestScope.baseCurrency.abbreviation}">
                                <c:if test="${currency.value != null}">
                                    <div class="row">
                                        <div class="col"><c:out value="${currency.key}
                                            ${requestScope.baseCurrency.abbreviation}"/></div>
                                        <div class="col"><c:out value="${currency.value}"/></div>
                                    </div>
                                    <br/>
                                </c:if>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>

