<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Accounts > Block Account</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/accountsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <h1>Block account page</h1>
                </div>
            </div>
        </div>
    </div>


</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>