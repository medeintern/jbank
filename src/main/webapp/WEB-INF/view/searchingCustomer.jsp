<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">
    <div class="container">
        <form action="${pageContext.request.contextPath}/secured/searching" method="post">
            <h2 class="text-center">Search Customers Accounts</h2>

            <div class="col-sm-12">
                <hr>
            </div>

            <div class="form-row text-center">
                <div class="col">
                    <label class="font-weight-bold">Login</label>
                    <input type="text" class="form-control" placeholder="Login" name="login">
                </div>

                <div class="col">
                    <label class="font-weight-bold">First Name</label>
                    <input type="text" class="form-control" placeholder="First Name" name="firstName">
                </div>

                <div class="col">
                    <label class="font-weight-bold">Last Name</label>
                    <input type="text" class="form-control" placeholder="Last Name" name="lastName">
                </div>

                <div class="col">
                    <label class="font-weight-bold">Account</label>
                    <input type="text" class="form-control" placeholder="Account" name="account">
                </div>
            </div>
            <div class="container center-block col-sm-2" style="margin-top: 1%">
                <button type="submit" class="btn btn-outline-primary col">Search</button>
            </div>
        </form>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <c:if test="${accountDtoList != null}">
            <table id="myTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Login</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Currency Abbreviation</th>
                    <th scope="col">Account Number</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="account" items="${accountDtoList}" varStatus="count">

                    <tr>
                        <th><c:out value="${count.count}"/></th>
                        <td><c:out value="${account.customerLogin}"/></td>
                        <td><c:out value="${account.customerName}"/></td>
                        <td><c:out value="${account.customerSurname}"/></td>
                        <td><c:out value="${account.currencyAbbreviation}"/></td>
                        <td><c:out value="${account.accountNumber}"/></td>
                        <input type="hidden" name="number" value="${account.accountNumber}">
                        <td>
                            <form action="${pageContext.request.contextPath}/secured/searching" method="post">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" style="width: auto" name="accTo"
                                            value="${account.accountNumber}">Transfer
                                    </button>
                                </div>
                            </form>

                            <form action="${pageContext.request.contextPath}/secured/searching" method="post">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" style="width: auto" name="accFrom"
                                            value="${account.accountNumber}">Claim
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

</body>
</html>
