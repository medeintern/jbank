<%@ page contentType="text/html;charset=UTF-8" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Transactions > Pending Claims</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/transactionsSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <table id="pendingClaimsTable" class="table table-striped table-bordered text-center"
                           style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Currency</th>
                            <th>Amount</th>
                            <th>Timestamp</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="transaction" items="${transactionDtoList}" varStatus="count">
                            <tr>
                                <th>${count.count}</th>
                                <td>${transaction.customerName}</td>
                                <td>${transaction.customerSurname}</td>
                                <td>${transaction.currencyAbbreviation}</td>
                                <td>${transaction.amount}</td>
                                <td class="date-td">${transaction.transactionDate}</td>
                                <td>
                                    <form action="${pageContext.request.contextPath}/secured/pendingClaims"
                                          method="post">
                                        <input type="hidden" value="${transaction.transactionId}" name="transactionId">
                                        <input type="submit" class="btn btn-danger" name="cancel" value="Cancel">
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(document).ready(function () {
        $('#pendingClaimsTable').DataTable({
            "scrollX": true
        })
    });

    $('.date-td').each(function () {
        this.innerHTML = convertDateFromUTC(this.innerHTML);
    });
</script>
</body>
</html>
