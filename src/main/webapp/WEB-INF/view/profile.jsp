<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>
<c:if test="${customer != null}">
    <c:set var="customer" value='${requestScope["customer"]}'/>
    <c:set var="error" value='${requestScope["error"]}'/>

    <c:set var="id" value='${customer.getId()}'/>
    <c:set var="login" value='${customer.getLogin()}'/>
    <c:set var="email" value='${customer.getEmail()}'/>
    <c:set var="name" value='${customer.getName()}'/>
    <c:set var="surname" value='${customer.getSurname()}'/>
    <c:set var="birthdate" value='${customer.getBirthdate()}'/>
    <c:set var="address" value='${customer.getAddress()}'/>
    <c:set var="state" value='${customer.getGender().toString()}'/>
</c:if>

<div class="container registerContainer">
    <h2 class="text-left">Edit Profile</h2>

    <div class="col-sm-12">
        <hr>
    </div>

    <c:if test="${error != null}">

        <c:forEach var="num" items="${error}">
            <ul>
                <li>${num}</li>
            </ul>
        </c:forEach>
        <div class="col-sm-12">
            <hr>
        </div>

    </c:if>

    <div class="row">

        <div class="col-sm-10">
            <form action="/secured/profile" method="post" enctype="multipart/form-data">

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Login</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="login" value="<c:out value="${login}"/>" readonly
                               required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Current Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="currentPassword"
                               placeholder="Current Password" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">New Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="newPassword" placeholder="New Password">
                        <small class="form-text text-muted">Password must have 8-40 symbols. At least: one or more
                            letters (a-z, A-Z), one or more digits, one or more special symbols(@#$%!), no whitespaces
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Retype Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="retypePassword" placeholder="Retype Password">
                        <small id="emailHelp" class="form-text text-muted">Password must have 8-40 symbols. At least:
                            one or more letters (a-z, A-Z), one or more digits, one or more special symbols(@#$%!), no
                            whitespaces
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" value="<c:out value="${email}"/>" name="email"
                               placeholder="Email" required>
                        <small class="form-text text-muted">Email should have a standard form</small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">First Name</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="firstName" value="<c:out value="${name}"/>"
                               placeholder="First Name" required>
                        <small class="form-text text-muted">Name shouldn`t be shorter than 2 symbols and longer than 45.
                            And contains a-Z, A-Z letters, digits, - and _ symbols
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Last Name</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="lastName" value="<c:out value="${surname}"/>"
                               placeholder="Last Name" required>
                        <small class="form-text text-muted">Last Name shouldn`t be shorter than 2 symbols and longer
                            than
                            45. And contains a-Z, A-Z letters, digits, - and _ symbols
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Gender</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="state" value="<c:out value="${state}"/>" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Address</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="address" value="<c:out value="${address}"/>"
                               placeholder="Address" required>
                        <small class="form-text text-muted">Address shouldn`t be shorter 10 symbols and longer than 255.
                            And must contain a-z A-Z letters, digits, and symbols -_.,
                        </small>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Birthday</label>
                    <div class="col-9">
                        <input class="form-control" type="date" name="birthdate" value="<c:out value="${birthdate}"/>"
                               id="example-date-input" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Photo</label>
                    <div class="col-9">
                        <input class="filestyle" type="file" name="photoFile">
                    </div>
                </div>

                <button type="submit" class="btn btn-group-sm btn-primary">Submit</button>
            </form>
        </div>

        <div class="col-sm-2">
            <img alt="alt2" class="img-fluid" src="/secured/photo?id=${id}"/>
        </div>

    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>

</html>