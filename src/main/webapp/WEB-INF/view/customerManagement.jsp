<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">

    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Admin > Customer Management</h4>
            <hr>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <%@ include file="/WEB-INF/jspf/adminSideBar.jspf" %>
                </div>
                <div class="col-9">
                    <div class="row" style="height: 50%">
                        <form class="form-inline"
                              action="${pageContext.request.contextPath}/secured/admin/customerManagement"
                              method="post">
                            <div class="form-group mx-sm-3 mb-2">
                                <input type="text" class="form-control col" name="login"
                                       placeholder="Login">
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <input type="text" class="form-control col" name="email"
                                       placeholder="Email">
                            </div>
                            <button type="submit" class="btn btn-success mb-2">Search</button>
                            <br>
                        </form>
                    </div>

                    <div class="row" style="height: 50%">
                        <div><h3>Customer:</h3></div>
                        <table class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Login</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Active</th>
                                <th>Blocked</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${customer != null}">
                                <tr>
                                    <td><c:out value="${customer.name}"/></td>
                                    <td><c:out value="${customer.surname}"/></td>
                                    <td><c:out value="${customer.login}"/></td>
                                    <td><c:out value="${customer.email}"/></td>
                                    <td><c:out value="${customer.address}"/></td>
                                    <td><c:out value="${customer.isActive}"/></td>
                                    <td>
                                        <form method="post"
                                              action="${pageContext.request.contextPath}/secured/admin/customerChangeBlocked">
                                            <c:if test="${customer.isBlocked eq 'false'}">
                                                <button name="customerBlocked" type="submit"
                                                        class="btn btn-success mb-2">
                                                        ${customer.isBlocked}</button>
                                            </c:if>

                                            <c:if test="${customer.isBlocked eq 'true'}">
                                                <button name="customerBlocked" type="submit"
                                                        class="btn btn-danger mb-2">
                                                        ${customer.isBlocked}</button>
                                            </c:if>

                                            <input type="hidden" name="id" value="${customer.id}">
                                            <input type="hidden" name="login" value="${customer.login}">
                                            <input type="hidden" name="blocked" value="${customer.isBlocked}">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="${pageContext.request.contextPath}/secured/admin/accountsBlocked"
                                              method="post">
                                            <input type="hidden" name="customerId" value="${customer.id}">
                                            <input type="hidden" name="login" value="${customer.login}">
                                            <button type="submit" class="btn btn-success mb-2">
                                                Show Accounts
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:if>
                            </tbody>
                        </table>
                    </div>

                    <div class="row" style="height: 50%">
                        <c:if test="${accountDtoList != null}">
                            <div><h3>Accounts:</h3></div>
                            <table class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Login</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Currency Abbreviation</th>
                                    <th scope="col">Balance</th>
                                    <th scope="col">Account Number</th>
                                    <th scope="col">Blocked</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="account" items="${accountDtoList}" varStatus="count">
                                    <tr>
                                        <th><c:out value="${count.count}"/></th>
                                        <td><c:out value="${account.customerLogin}"/></td>
                                        <td><c:out value="${account.customerName}"/></td>
                                        <td><c:out value="${account.customerSurname}"/></td>
                                        <td><c:out value="${account.currencyAbbreviation}"/></td>
                                        <td><c:out value="${account.balance}"/></td>
                                        <td><c:out value="${account.accountNumber}"/></td>
<%--                                        <input type="hidden" name="login" value="${account.customerLogin}">--%>
                                        <td>
                                            <form method="post"
                                                  action="${pageContext.request.contextPath}/secured/admin/customerChangeBlocked">
                                                <c:if test="${account.isBlocked eq 'false'}">
                                                    <button name="accountBlocked" type="submit"
                                                            class="btn btn-success mb-2">
                                                            ${account.isBlocked}</button>
                                                </c:if>

                                                <c:if test="${account.isBlocked eq 'true'}">
                                                    <button name="accountBlocked" type="submit"
                                                            class="btn btn-danger mb-2">
                                                            ${account.isBlocked}</button>
                                                </c:if>

                                                <input type="hidden" name="id" value="${account.accountId}">
                                                <input type="hidden" name="login" value="${customer.login}">
                                                <input type="hidden" name="customerId" value="${customer.id}">
                                                <input type="hidden" name="blocked" value="${account.isBlocked}">
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<script>
    $(document).ready(function () {
        $('table.display').DataTable({
            paging: false,
            "scrollX": true
        });
    });
</script>
</body>
</html>