<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container">

    <form method="post" action="${pageContext.request.contextPath}/secured/transactionDetails">
        <input type="hidden" name="transactionId" value="${transactionId}">
        <input type="hidden" name="whereFrom" value="${whereFrom}">
        <div class="card">
            <article class="card-body">
                <h4 class="card-title text-center mb-4 mt-1">Confirm transaction:</h4>
                <hr>
                <div class="form-group col-8">
                    <label for="fromAcc">
                        <i><c:out value="${transDto.senderName} ${transDto.senderSurname}"/></i></label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="numberSend">Sender: </span>
                        </div>
                        <input type="text" disabled="true" class="form-control" id="fromAcc"
                               aria-describedby="numberSend"
                               value="${transDto.senderAccountNumber}">
                    </div>
                </div>
                <div class="form-group col-8">
                    <label for="toAcc">
                        <i><c:out value="${transDto.recipientName} ${transDto.recipientSurname}"/></i></label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="numberRec">Recipient:</span>
                        </div>
                        <input type="text" disabled="true" class="form-control" id="toAcc" aria-describedby="numberRec"
                               value="${transDto.recipientAccountNumber}">
                    </div>
                </div>

                <div class="input-group mb-3 col-2">
                    <input type="text" disabled="true" class="form-control" aria-label="Amount"
                           value="${transDto.amount}">
                    <div class="input-group-append">
                        <span class="input-group-text"><i><c:out value="${transDto.currencyAbbreviation}"/></i></span>
                    </div>
                </div>

                <button type="submit" class="btn btn-outline-primary col-1" name="confirm" , value="Confirm">
                    Confirm
                </button>
                <button type="submit" class="btn btn-outline-danger col-1" name="cancel" value="Cancel">Cancel
                </button>
            </article>
        </div>
    </form>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>