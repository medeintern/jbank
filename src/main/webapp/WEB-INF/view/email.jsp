<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isErrorPage="true" %>
<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="form">
    <h4> You have to click the verification link (from the email we have sent you) to activate your account. </h4>
    <h6> If there is not any message, call our customer department, please. </h6>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>
