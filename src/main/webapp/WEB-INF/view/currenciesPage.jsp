<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>

<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-left mb-4 mt-1">Available currencies</h4>
            <hr>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-8">
                    <table id="balanceTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Currency code</th>
                            <th scope="col">Currency name</th>
                            <th scope="col">JCN per Unit</th>
                            <th scope="col">Units per JCN</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="currency" items="${currencies}" varStatus="count">
                            <tr>
                                <th scope="row"><c:out value="${count.count}"/></th>
                                <td><c:out value="${currency.abbreviation}"/></td>
                                <td><c:out value="${currency.name}"/></td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="4"
                                                      value="${currency.rate}"/></td>
                                <td><fmt:formatNumber type="number" maxFractionDigits="4"
                                                      value="${1/currency.rate}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
<script>
    $(document).ready(function () {
        $('#balanceTable').DataTable();
    });
</script>
</body>
</html>