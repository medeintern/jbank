package com.mede.jbank.servlet;

import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Gender;
import com.mede.jbank.validator.JbValidator;
import com.mede.jbank.validator.JbValidatorFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class ProfilePageControllerTest {
    private Customer customer = new Customer();
    private final ProfilePageController controller = new ProfilePageController();
    private Set<String> destinationSet;
    private JbValidator validator;
    private final String originalPassword =
            "2530e9f59e3e3af3701c4fb990d1040dac993b80ab3d237be5d9b55c604d15c8312031e0b1ad4795c8f7d25e25e1818322b4fb96757f9512d9736a7b9e017e02";
    private Customer modifiedCustomer;

    @Before
    public void init() {
        validator = JbValidatorFactory.INSTANCE.getValidator();
        destinationSet = new HashSet<>();
        customer.setId(1L);
        customer.setName("Admin");
        customer.setSurname("FirstAdmin");
        LocalDate localDate = LocalDate.of(1980, 1, 1);
        customer.setBirthdate(localDate);
        customer.setEmail("admin@admindomain.com");
        customer.setRegistrationDate(LocalDateTime.of(2019, 4, 25, 18, 14, 25));
        customer.setAddress("Kharkiv");
        customer.setLogin("admin");
        customer.setPassword("2530e9f59e3e3af3701c4fb990d1040dac993b80ab3d237be5d9b55c604d15c8312031e0b1ad4795c8f7d25e25e1818322b4fb96757f9512d9736a7b9e017e02");
        customer.setIsBlocked(false);
        customer.setIsActive(true);
        customer.setGender(Gender.UNDEFINED);
    }


    @Test
    public void correctOrIncorrectCurrentPasswordTest() {
        Customer modifiedCustomer = checkParameters("jB@nkAdm1n", "", "");
        Assert.assertEquals(0, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        clearErrorSet();

        modifiedCustomer = checkParameters("jB1117SAW", "", "");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void emptyPasswordTest() {
        modifiedCustomer = checkParameters("", "", "");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();

        modifiedCustomer = checkParameters("null", "null", "null");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void differentNewPasswordsCorrectCurrentPassword() {
        modifiedCustomer = checkParameters("jB@nkAdm1n", "2Asw7;ok", "");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and retype different"));
        clearErrorSet();

        modifiedCustomer = checkParameters("jB@nkAdm1n", "", "trh55Wq");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and retype different"));
        clearErrorSet();

        modifiedCustomer = checkParameters("jB@nkAdm1n", "874HYtg54ed", "trh55Wq");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and retype different"));
        clearErrorSet();

        modifiedCustomer = checkParameters("jB@nkAdm1n", "jB@nkAdm1n", "trh55Wq");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and retype different"));
        clearErrorSet();
    }

    @Test
    public void differentNewPasswordsIncorrectCurrentPassword() {
        modifiedCustomer = checkParameters("777tgjh888", "", "trh55Wq");
        Assert.assertEquals(2, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and retype different"));
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void newPasswordsEqualsCurrent() {
        modifiedCustomer = checkParameters("jB@nkAdm1n", "jB@nkAdm1n", "jB@nkAdm1n");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("New password and current are the same"));
        clearErrorSet();

        modifiedCustomer = checkParameters("jB@nkAdm1n1", "jB@nkAdm1n1", "jB@nkAdm1n1");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void correctNewPasswordsWrongCurrent() {
        modifiedCustomer = checkParameters("jB@nkAdm1n777", "14asdEb45@", "14asdEb45@");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void newTheSameCurrentButWrongCurrent() {
        modifiedCustomer = checkParameters("jB@nkAdm1n777", "jB@nkAdm1n", "jB@nkAdm1n");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertEquals(originalPassword, modifiedCustomer.getPassword());
        Assert.assertTrue(destinationSet.contains("Wrong current password"));
        clearErrorSet();
    }

    @Test
    public void incorrectNewPasswords() {
        modifiedCustomer = checkParameters("jB@nkAdm1n", "qwerTy117", "qwerTy117");
        Assert.assertEquals(1, destinationSet.size());
        Assert.assertTrue(destinationSet.contains("Password must have 8-40 symbols. " +
                "At least: one or more letters (a-z, A-Z), one or more digits, one or more special symbols(@#$%!), no whitespaces"));
        Assert.assertNotEquals(originalPassword, modifiedCustomer.getPassword());
        clearErrorSet();
    }

    private Customer checkParameters(String currentPassword, String newPassword, String repeatPassword) {
        customer = controller.checkPasswords(customer, currentPassword, newPassword, repeatPassword, destinationSet, validator);
        return customer;
    }

    private void clearErrorSet() {
        destinationSet.clear();
    }

}
