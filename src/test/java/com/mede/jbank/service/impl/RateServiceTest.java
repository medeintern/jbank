package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Currency;
import com.mede.jbank.entity.Rate;
import com.mede.jbank.service.ServiceContext;
import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDateTime;

@Log4j2
public class RateServiceTest {

    private static RateService rateService;
    private static CurrencyService currencyService;
    private static Rate rate;
    private static Currency currency;
    @Rule
    public PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        rateService = context.getService(RateService.class);
        currencyService = context.getService(CurrencyService.class);
        currency = new Currency();
        currency.setName("Name");
        currency.setIsBase(false);
        currency.setAbbreviation("ABBR");
        currency.setRate(2D);
        rate = new Rate();
        rate.setRate(3D);
        rate.setStartDate(LocalDateTime.now());
    }

    @Test
    public void insertNextRateAndSetEndPreviousSetCurrencyRate() {
        int numberRates = rateService.findAllRates().size();
        Assert.assertTrue(numberRates > 0);
        Currency savedCurrency = currencyService.insertNewCurrency(this.currency);
        long idNewCurrency = savedCurrency.getId();
        int numberRatesAfterAddNewCurrency = rateService.findAllRates().size();
        Assert.assertEquals(numberRates + 1, numberRatesAfterAddNewCurrency);
        Assert.assertEquals(savedCurrency.getName(), currency.getName());
        Assert.assertEquals(savedCurrency.getAbbreviation(), currency.getAbbreviation());
        Assert.assertEquals(savedCurrency.getIsBase(), currency.getIsBase());
        Assert.assertEquals(savedCurrency.getRate(), currency.getRate());
        rate.setCurrencyId(savedCurrency.getId());
        rateService.insertNextRateAndSetEndPreviousSetCurrencyRate(rate);
        int numberRatesAfterAddNewRate = rateService.findAllRates().size();
        Assert.assertEquals(numberRatesAfterAddNewCurrency + 1, numberRatesAfterAddNewRate);
        Currency currencyAfterChangeRate = currencyService.findCurrencyById(idNewCurrency);
        Assert.assertEquals(rate.getRate(), currencyAfterChangeRate.getRate());
    }
}