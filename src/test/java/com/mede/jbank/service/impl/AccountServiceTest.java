package com.mede.jbank.service.impl;

import com.mede.jbank.entity.Account;
import com.mede.jbank.service.ServiceContext;
import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;

@Log4j2
public class AccountServiceTest {

    private static AccountService accountService;
    private static Account account = new Account();

    @Rule
    public PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        accountService = context.getService(AccountService.class);
        account.setCustomerId(1L);
        account.setCurrencyId(2L);
        account.setAmount(BigDecimal.valueOf(0));
        account.setIsBlocked(false);
    }

    @Test
    public void testSaveAccount() {
        Account saved = accountService.saveAccount(account, 1L);
        Assert.assertEquals(saved.getCurrencyId(), account.getCurrencyId());
        Assert.assertEquals(saved.getCustomerId(), account.getCustomerId());
        Assert.assertEquals(saved.getIsBlocked(), account.getIsBlocked());
        Assert.assertEquals(saved.getAmount().compareTo(account.getAmount()), 0);
        Account saved1 = accountService.saveAccount(account, 1L);
        Assert.assertNull(saved1);
    }
}