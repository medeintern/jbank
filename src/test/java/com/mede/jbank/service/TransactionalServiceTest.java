package com.mede.jbank.service;

import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Transaction;
import com.mede.jbank.entity.enums.State;
import com.mede.jbank.service.impl.AccountService;
import com.mede.jbank.service.impl.TransactionService;
import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Log4j2
public class TransactionalServiceTest {

    private static TransactionService transactionService;
    private static AccountService accountService;

    @Rule
    public PreparedDbRule db =
            EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/testChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        transactionService = context.getService(TransactionService.class);
        accountService = context.getService(AccountService.class);
    }

    @Test
    public void transferTest() {
        List<Transaction> transactionList = transactionService.getTransactionListByAccountId(1L);

        int count = transactionList.size();
        CountDownLatch latch = new CountDownLatch(count);

        ExecutorService executor = Executors.newFixedThreadPool(10);
        try {
            transactionList.forEach(t -> executor.submit(() -> {
                transactionService.transfer(t);
                log.info("Transaction is processing: {}", t);
                latch.countDown();
            }));
            latch.await();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        log.info("main continue work");

        Account senderAccount = accountService.findAccountById(1L);
        Account recipientAccount = accountService.findAccountById(3L);

        int countFailedTransaction = (int) transactionService.getListTransactions(1L).stream()
                .filter(el -> el.getState() == State.FAILED)
                .count();

        int countCompletedTransaction = (int) transactionService.getListTransactions(1L).stream()
                .filter(el -> el.getState() == State.COMPLETED)
                .count();

        Assert.assertEquals(0, senderAccount.getAmount().intValue());
        Assert.assertEquals(6000, recipientAccount.getAmount().intValue());

        Assert.assertEquals(1, countFailedTransaction);
        Assert.assertEquals(5, countCompletedTransaction);

    }
}
