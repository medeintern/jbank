package com.mede.jbank.service;

import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Gender;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.impl.CustomerService;

import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CustomerServiceTest {
    private static CustomerService customerService;
    private static Customer customer = new Customer();

    @Rule
    public PreparedDbRule db =
            EmbeddedPostgresRules.preparedDatabase(
            LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        customerService = context.getService(CustomerService.class);

        customer.setId(1L);
        customer.setName("Admin");
        customer.setSurname("FirstAdmin");

        LocalDate localDate = LocalDate.of(1980, 1, 1);
        customer.setBirthdate(localDate);

        customer.setEmail("admin@admindomain.com");
        customer.setRegistrationDate(LocalDateTime.of(2019, 4, 25, 18, 14, 25));
        customer.setAddress("Kharkiv, Moskovsky 190");
        customer.setLogin("admin");
        customer.setPassword("2530e9f59e3e3af3701c4fb990d1040dac993b80ab3d237be5d9b55c604d15c8312031e0b1ad4795c8f7d25e25e1818322b4fb96757f9512d9736a7b9e017e02");
        customer.setIsBlocked(false);
        customer.setIsActive(true);
        customer.setGender(Gender.UNDEFINED);
        customer.setEmailCode(UUID.fromString("137b920e-3460-4b1a-a123-ebe23277cd73"));
    }

    @Test
    public void findCustomerByIdTest() {
        Customer dbCustomer = customerService.findCustomerById(customer.getId());

        Assert.assertEquals(customer, dbCustomer);
    }

    @Test
    public void findCustomerByLoginTest() {
        Customer dbCustomer = customerService.findCustomerByLogin(customer.getLogin());

        Assert.assertEquals(customer, dbCustomer);
    }

    @Test
    public void addCustomerTest() {
        Customer newCustomer = customer;
        newCustomer.setLogin("testLogin");
        newCustomer.setEmail("testEmail@user.com");
        Account account = new Account();
        account.setCustomerId(customer.getId());
        account.setCurrencyId(1L);
        account.setAmount(BigDecimal.valueOf(0L));
        account.setIsBlocked(false);

        Assert.assertNotNull(customerService.addCustomer(newCustomer, Role.CUSTOMER, account));
    }

    @Test
    public void getListCustomersTest() {
        List<Customer> list = Arrays.asList(customer);

        Assert.assertEquals(list, customerService.findListCustomers());
    }

    @Test
    public void updateCustomerTest() {
        customer.setName("newAdmin");
        customer.setSurname("newSurname");
        customer.setEmail("newEmail@test.ua");
        customer.setAddress("newLocation");
        customer.setPassword("aslkgjalgalngalingi343");

        Assert.assertTrue(customerService.updateCustomer(customer));
    }

    @Test
    public void activateTest() {
        Assert.assertTrue(customerService.activateByCode(customer.getEmailCode()));
    }

    @Test
    public void changeBlockedTest() {
        Assert.assertTrue(customerService.changeBlocked(customer.getId(), !customer.getIsBlocked()));
    }

    @Test
    public void findCustomerByEmailTest() {
        Customer dbCustomer = customerService.findCustomerByEmail(customer.getEmail());

        Assert.assertEquals(customer, dbCustomer);
    }

    @Test
    public void isExistCustomerByLoginOrEmailTest() {
        Assert.assertTrue(customerService.isExistCustomerByLoginOrEmail(customer));

        customer.setLogin("anotherLogin");
        Assert.assertTrue(customerService.isExistCustomerByLoginOrEmail(customer));

        customer.setEmail("anotherEmail");
        Assert.assertFalse(customerService.isExistCustomerByLoginOrEmail(customer));
    }
}
