package com.mede.jbank.service;

import com.mede.jbank.entity.Account;
import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Gender;
import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.repository.CustomerRepository;
import com.mede.jbank.repository.CustomerRoleRepository;
import com.mede.jbank.service.impl.CustomerService;

import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.transaction.SerializableTransactionRunner;
import org.jdbi.v3.core.transaction.TransactionIsolationLevel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.*;

@Log4j2
public class JdbiTest {
    private static Customer customer = new Customer();
    private static CustomerService customerService;
    private static Jdbi jdbi;
    private static Account account = new Account();

    @Rule
    public PreparedDbRule db =
            EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        customerService = context.getService(CustomerService.class);
        jdbi = context.getJdbi();

        customer.setName("customer");
        customer.setSurname("SurnameCustomer");

        LocalDate localDate = LocalDate.of(1980, 1, 1);
        customer.setBirthdate(localDate);

        customer.setEmail("customer@customerDomain.com");
        customer.setRegistrationDate(LocalDateTime.of(2019, 4, 25, 18, 14, 25));
        customer.setAddress("Kharkov, Moscow ave 222");
        customer.setLogin("customer");
        customer.setPassword("2530e9f59e3e3af3701c4fb990d1040dac993b80ab3d237be5d9b55c604d15c8312031e0b1ad4795c8f7d25e25e1818322b4fb96757f9512d9736a7b9e017e02");
        customer.setIsBlocked(false);
        customer.setIsActive(false);
        customer.setGender(Gender.MALE);
        account.setIsBlocked(false);
        account.setAmount(BigDecimal.ZERO);
        account.setCurrencyId(1L);
    }

    @Test
    public void insertCustomerJdbiTest() {

        Assert.assertFalse(customerService.isExistCustomerByLoginOrEmail(customer));

        customerService.addCustomer(customer, Role.CUSTOMER, account);

        Assert.assertTrue(customerService.isExistCustomerByLoginOrEmail(customer));
    }

    @Test
    public void insertCustomerJdbiWhereExceptionTest() {

        Assert.assertFalse(customerService.isExistCustomerByLoginOrEmail(customer));

        try {
            jdbi.inTransaction(handle -> {
                CustomerRepository customerRepo = handle.attach(CustomerRepository.class);
                CustomerRoleRepository customerRoleRepo = handle.attach(CustomerRoleRepository.class);
                Customer newCustomer = customerRepo.insert(customer);
                boolean flag = customerRoleRepo.insert(newCustomer.getId(), Role.CUSTOMER);
                handle.close();
                return flag;
            });
        } catch (Exception ex) {
            log.info("Exception in transaction. Connection has been closed");
        }

        Assert.assertFalse(customerService.isExistCustomerByLoginOrEmail(customer));
    }

    @Test
    public void multipleIdenticalCustomersInsertTest() {
        try {
            for (int i = 0; i < 5; i++) {
                JdbiTestThread jdbiTestThread = new JdbiTestThread();
                jdbiTestThread.start();
                jdbiTestThread.join();
            }
        } catch (Exception ex) {
            log.info("Insert Exception. Login or email already exist");
        }

        Integer count = jdbi.withHandle(handle -> handle.createQuery("SELECT count(*) from customers WHERE login = (:login)")
                .bind("login", customer.getLogin())
                .mapTo(Integer.class)
                .findOnly());

        Assert.assertEquals(new Integer(1), count);
    }

    private static class JdbiTestThread extends Thread {
        @Override
        public void run() {
            customerService.addCustomer(customer, Role.CUSTOMER, account);
        }
    }

    @Test
    public void serializableTest() throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newCachedThreadPool();

        jdbi.setTransactionHandler(new SerializableTransactionRunner());

        Callable<Integer> callable = () -> {
            FakeValuesService faker = new FakeValuesService(new Locale("en-GB"), new RandomService(new Random(Thread.currentThread().getId())));
            return jdbi.inTransaction(TransactionIsolationLevel.SERIALIZABLE, handle -> {

                String email = faker.bothify("????##@gmail.com");
                String login = faker.regexify("[a-z1-9]{10}");

                Customer anotherCustomer = new Customer();
                Account account2 = new Account();
                anotherCustomer.setName("customer");
                anotherCustomer.setSurname("SurnameCustomer");

                LocalDate localDate = LocalDate.of(1980, 1, 1);
                anotherCustomer.setBirthdate(localDate);
                anotherCustomer.setEmail(email);
                anotherCustomer.setRegistrationDate(LocalDateTime.of(2019, 4, 25, 18, 14, 25));
                anotherCustomer.setAddress("Kharkov, Moscow ave 222");
                anotherCustomer.setLogin(login);
                anotherCustomer.setPassword("2530e9f59e3e3af3701c4fb990d1040dac993b80ab3d237be5d9b55c604d15c8312031e0b1ad4795c8f7d25e25e1818322b4fb96757f9512d9736a7b9e017e02");
                anotherCustomer.setIsBlocked(false);
                anotherCustomer.setIsActive(false);
                anotherCustomer.setGender(Gender.MALE);
                account2.setIsBlocked(false);
                account2.setAmount(BigDecimal.ZERO);
                account2.setCurrencyId(1L);

                if (!customerService.isExistCustomerByLoginOrEmail(customer)) {
                    log.info("Thread {} is sleeping...", Thread.currentThread().getName());
                    Thread.sleep(1000);
                    log.info("Thread {} woke up !", Thread.currentThread().getName());
                    customerService.addCustomer(anotherCustomer, Role.MANAGER, account2);
                }
                customerService.findListCustomers().forEach(log::info);
                int countCustomers = handle.createQuery("SELECT count(*) FROM customers").mapTo(Integer.class).findOnly();
                return countCustomers;
            });
        };

        Future<Integer> f1 = executorService.submit(callable);
        Future<Integer> f2 = executorService.submit(callable);

        int result = f1.get() + f2.get();
        executorService.shutdown();

        Assert.assertEquals(2 + 3, result);
    }
}
