package com.mede.jbank.service;

import com.mede.jbank.entity.enums.Role;
import com.mede.jbank.service.impl.CustomerRoleService;
import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CustomerRoleServiceTest {
    private static CustomerRoleService customerRoleService;

    @Rule
    public PreparedDbRule db =
            EmbeddedPostgresRules.preparedDatabase(
            LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        customerRoleService = context.getService(CustomerRoleService.class);
    }

    @Test
    public void findRolesByCustomerIdTest() {
        List<Role> roleList = Arrays.asList(Role.MANAGER);

        Assert.assertEquals(roleList, customerRoleService.findRolesByCustomerId(1L));
    }
}
