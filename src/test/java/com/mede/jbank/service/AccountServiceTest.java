package com.mede.jbank.service;

import com.mede.jbank.dto.AccountDto;
import com.mede.jbank.entity.Account;
import com.mede.jbank.service.impl.AccountService;

import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;

public class AccountServiceTest {

    private static AccountService accountService;
    private static Account account = new Account();

    @Rule
    public PreparedDbRule db =
            EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/testChangeLog.xml"));

    @Before
    public void init() {
        ServiceContext context = new ServiceContext(db.getTestDatabase());
        accountService = context.getService(AccountService.class);

        account.setId(1L);
        account.setNumber("1231312132");
        account.setCustomerId(1L);
        account.setCurrencyId(1L);
        account.setAmount(BigDecimal.valueOf(1000).setScale(2));
        account.setIsBlocked(false);

    }

    @Test
    public void findAccountByNumber() {
        Account dbAccount = accountService.findAccountByNumber(account.getNumber());
        Assert.assertEquals(account, dbAccount);
    }

    @Test
    public void findAccountByIdTest() {
        Account dbAccount = accountService.findAccountById(1L);
        Assert.assertEquals(account, dbAccount);
    }

    @Test
    public void findWorkingAccountByNumberTest() {
        Long dbAccountId = accountService.findWorkingAccountByNumber(account.getNumber());
        Assert.assertEquals(account.getId(), dbAccountId);
    }

    @Test
    public void findAdminBaseAccountNumberTest() {
        String adminBaseAccountNumber = accountService.findBaseAccountDtoByCustomerId(1L).getAccountNumber();
        Assert.assertEquals(adminBaseAccountNumber, accountService.findAdminBaseAccountNumber());
    }

    @Test
    public void isBaseAccountTest() {
        Assert.assertTrue(accountService.isBaseAccount("1231312132"));
    }

    @Test
    public void findAccountDtoByNumber() {
        AccountDto accountDto = new AccountDto();

        accountDto.setAccountId(3L);
        accountDto.setAccountNumber("1121212211");
        accountDto.setCustomerLogin("user");
        accountDto.setCustomerName("User");
        accountDto.setCustomerSurname("FirstUser");
        accountDto.setCurrencyAbbreviation("JCN");
        accountDto.setIsBase(true);
        accountDto.setBalance(BigDecimal.valueOf(5000).setScale(2));
        accountDto.setIsBlocked(false);

        Assert.assertEquals(accountDto, accountService.findAccountDtoByNumber("1121212211"));
    }
}
