package com.mede.jbank.jwt;

import com.mede.jbank.entity.enums.Role;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;

public class JwTokenHelperTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void shouldAddPrincipal() {
        Principal principal = new Principal();
        principal.setLogin("login");
        principal.setName("Ivan");
        principal.setRoles(Arrays.asList(Role.CUSTOMER));

        String jwt = JwTokenHelper.createJwt(principal);

        Assert.assertNotNull(jwt);
        Assert.assertEquals(JwTokenHelper.parseJwt(jwt), principal);
    }

    @Test
    public void shouldThrowJwtExceptionWrongToken() {
        Principal principal = new Principal();
        principal.setLogin("login");
        principal.setName("Ivan");
        principal.setRoles(Arrays.asList(Role.CUSTOMER));

        exception.expect(Exception.class);

        String jwt = JwTokenHelper.createJwt(principal);
        String wrongJwt = jwt + "abc";
        JwTokenHelper.parseJwt(wrongJwt);
    }

    @Test
    public void shouldThrowJwtExceptionOldToken() {
        Principal principal = new Principal();
        principal.setLogin("login");
        principal.setName("Ivan");
        principal.setRoles(Arrays.asList(Role.CUSTOMER));

        String jwt = JwTokenHelper.createJwt(principal, 0);

        exception.expect(Exception.class);

        Assert.assertNotNull(jwt);
        JwTokenHelper.parseJwt(jwt);
    }


}
