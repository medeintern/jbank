package com.mede.jbank.embeddedpg;

import com.opentable.db.postgres.embedded.LiquibasePreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class PgEmbeddedTest {

    @Rule
    public PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(LiquibasePreparer.forClasspathLocation("liquibase/outputChangeLog.xml"));

    @Test
    public void testRoles() throws Exception {
        int numberRows = findNumberRow("SELECT COUNT(*) as rowcount FROM roles");
        Assert.assertEquals(1, numberRows);
    }

    @Test
    public void testCustomers() throws Exception {
        int numberRows = findNumberRow("SELECT COUNT(*) as rowcount FROM customers");
        Assert.assertEquals(1, numberRows);
    }

    @Test
    public void testCurrencies() throws Exception {
        int numberRows = findNumberRow("SELECT COUNT(*) as rowcount FROM currencies");
        Assert.assertEquals(3, numberRows);
    }

    private int findNumberRow(String query) throws Exception {
        int idCounter = 0;
        try (Connection connection = db.getTestDatabase().getConnection();
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(query)
        ) {
            while (rs.next()) {
                idCounter = rs.getInt("rowcount");
            }
        }
        return idCounter;
    }
}
