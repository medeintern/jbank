package com.mede.jbank.validator;

import com.mede.jbank.entity.Customer;
import com.mede.jbank.entity.enums.Gender;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Log4j2
public class JbValidatorTest {

    JbValidator validator = JbValidatorFactory.INSTANCE.getValidator();
    Customer customer;

    @Before
    public void initCustomer() {
        customer = new Customer();
    }

    @Test
    public void fullCustomerCheck() {
        customer.setName("Alex");
        customer.setSurname("Smith");
        customer.setBirthdate(LocalDate.now().minusYears(25));
        customer.setRegistrationDate(LocalDateTime.now());
        customer.setLogin("AlexSmith");
        customer.setPassword("198@Akjh4TreaZ");
        customer.setAddress("USA, Texas, Dallas, Cowboy avenue 17");
        customer.setEmail("alex125@gmail.com");
        customer.setGender(Gender.MALE);
        Assert.assertEquals(0, validator.validate(customer).size());
        Assert.assertEquals(0, validator.validateOutSetString(customer).size());
        customer.setEmail(null);
        Assert.assertEquals(1, validator.validate(customer).size());
        Assert.assertEquals(1, validator.validateOutSetString(customer).size());
        customer.setAddress(null);
        Assert.assertEquals(2, validator.validate(customer).size());
        Assert.assertEquals(2, validator.validateOutSetString(customer).size());
        customer.setPassword("Alex123fox15");
        Assert.assertEquals(3, validator.validate(customer).size());
        Assert.assertEquals(3, validator.validateOutSetString(customer).size());
    }

    @Test
    public void nameFailTest() {
        //Fail cases
        customer.setName(null);//null
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("akjhkjgflkrlkjmthtbfthjyykiimkjihbvytvdfxybpupoukiok");//long
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("A");//short
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("John@");//@ symbol
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("John+John");//+ symbol
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("John John");//whitespace
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("");//empty
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("Мария");//
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("ВИКТОР");//
        Assert.assertEquals(1, numberFails(customer, "name"));
        customer.setName("捆当为梱");//
        Assert.assertEquals(1, numberFails(customer, "name"));
        //Ok
        customer.setName("Jennifer");//
        Assert.assertEquals(0, numberFails(customer, "name"));
        customer.setName("Anna-Sofia");//
        Assert.assertEquals(0, numberFails(customer, "name"));
        customer.setName("Vilgelm1");//
        Assert.assertEquals(0, numberFails(customer, "name"));
        customer.setName("Victor_Alexandr");//
        Assert.assertEquals(0, numberFails(customer, "name"));
    }

    @Test
    public void surnameFailTest() {
        //Fail cases
        customer.setSurname(null);//null
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("JohnAbdullaJasonAnnaMuurayDiegoDeLaCostaIvanAlexandrGregoryMiles");//long
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("A");//short
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("Li@");//@ symbol
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("Johnson+|Johnson");//+ symbol
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("Johnson Johnson");//whitespace
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("");//empty
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("Иванов");//
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("ВИКТЮК");//
        Assert.assertEquals(1, numberFails(customer, "surname"));
        customer.setSurname("捆当为梱");//
        Assert.assertEquals(1, numberFails(customer, "surname"));
        //Ok
        customer.setSurname("Jones");//
        Assert.assertEquals(0, numberFails(customer, "surname"));
        customer.setSurname("Ridley-Scott");//
        Assert.assertEquals(0, numberFails(customer, "surname"));
        customer.setSurname("Johnson1");//
        Assert.assertEquals(0, numberFails(customer, "surname"));
        customer.setSurname("Schneider_Vinsterden");//
        Assert.assertEquals(0, numberFails(customer, "surname"));

    }

    @Test
    public void birthdateFailTest() {
        //fail cases
        customer.setBirthdate(null);
        Assert.assertEquals(1, numberFails(customer, "birthdate"));
        customer.setBirthdate(LocalDate.now());
        Assert.assertEquals(1, numberFails(customer, "birthdate"));
        //ok
        customer.setBirthdate(LocalDate.now().minusYears(1));
        Assert.assertEquals(0, numberFails(customer, "birthdate"));
    }

    @Test
    public void addressFailTest() {
        //fail cases
        String longAddress = "London zdfxtdydxytxdytx txyrtxydr6gt dgr6d56r6 g56gvse65er6 se6vs5eb6s5e6vtxdytx txyrtxydr6gt dgr6d56r6 g56gvse65er6 se6vs5eb6s5e6v " +
                "vse56s5e6s5e6 wv56w565s36s5ertczsfsr6t sev6ws5e65se6 txdytx txyrtxydtxyrtxydr6gt dgr6dr6gt dgr6d56r6 g56gvse65er6 se6vs5eb6s5e6v";
        customer.setAddress(null);//null
        Assert.assertEquals(1, numberFails(customer, "address"));
        customer.setAddress(longAddress);//long
        Assert.assertEquals(1, numberFails(customer, "address"));
        customer.setAddress("");//empty
        Assert.assertEquals(1, numberFails(customer, "address"));
        customer.setAddress("London");//short
        Assert.assertEquals(1, numberFails(customer, "address"));
        //ok
        customer.setAddress("U.K. London, Baker street 16");
        Assert.assertEquals(0, numberFails(customer, "address"));
    }

    @Test
    public void registrationDateFailTest() {
        //fail cases
        customer.setRegistrationDate(null);
        Assert.assertEquals(1, numberFails(customer, "registrationDate"));
        customer.setRegistrationDate(LocalDateTime.now().plusDays(1));//future
        Assert.assertEquals(1, numberFails(customer, "registrationDate"));
        //ok
        customer.setRegistrationDate(LocalDateTime.now().minusHours(1));
        Assert.assertEquals(0, numberFails(customer, "registrationDate"));
        customer.setRegistrationDate(LocalDateTime.now());
        Assert.assertEquals(0, numberFails(customer, "registrationDate"));

    }

    @Test
    public void emailFailTest() {
        //Fail cases
        String longEmail = "1234567890abcd@abcdabcdabc.dabcdabcdabcdabcasvbfg.rtfhfyujfdabcdabcda.bcdabcdabcdcoma." +
                "cdabcdabcdabcda.bcdabcdabcdabcdabcda.bcdabcdabcdabcdcomabc.dabcdabcdabcdabcdcom.bcdabcdab" +
                "cdabcdabcda.bcdabcdab.aabcdabcdabcdabcdabcda.bcd.cdabcdabcdabcdabcdabcdabcda.com";//255 symbols
        customer.setEmail(longEmail + "z");//256 symbols
        Assert.assertEquals(1, numberFails(customer, "email"));
        customer.setEmail("example@@cv.com");//double @
        Assert.assertEquals(1, numberFails(customer, "email"));
        customer.setEmail("exa@mple@cv");//double @
        Assert.assertEquals(1, numberFails(customer, "email"));
        customer.setEmail("examplecv.com");//no @
        Assert.assertEquals(1, numberFails(customer, "email"));
        customer.setEmail(null);//null
        Assert.assertEquals(1, numberFails(customer, "email"));

        //ok
        customer.setEmail(longEmail);//255 symbols
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("12457@cv.com");
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("123почта-Имя@com.com");//Non ASCII
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("a@a.com");//one letter
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("Adf12-_'qw.t5@com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("Miles.O'Brian@example.com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("disposable.style.email.with+symbol@example.com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("other.email-with-hyphen@example.com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("fully-qualified-domain@example.com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("user.name+tag+sorting@example.com");//may go to user.name@example.com inbox depending on mail server
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("x@example.com");//one-letter local-part
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("example-indeed@strange-example.com");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("admin@mailserver1");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("example@s.example");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("\" \"@example.org");//
        Assert.assertEquals(0, numberFails(customer, "email"));
        customer.setEmail("\"john..doe\"@example.org");//
        Assert.assertEquals(0, numberFails(customer, "email"));
    }

    @Test
    public void loginFailTest() {
        //Fail cases
        customer.setLogin(null);//null
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("asdfe178901234567890123456789012345");//too long
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("asdf");//short
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("abc@1235");//@ symbol
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("--------");//without letters
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("11.54-8_9");//without letters
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("1askAde 9");//with whitespace
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("1256477");//only digits
        Assert.assertEquals(1, numberFails(customer, "login"));
        customer.setLogin("");//empty
        Assert.assertEquals(1, numberFails(customer, "login"));
        //Ok cases
        customer.setLogin("User_Newuser");//good
        Assert.assertEquals(0, numberFails(customer, "login"));
        customer.setLogin("a.sws-k_");//good
        Assert.assertEquals(0, numberFails(customer, "login"));
        customer.setLogin("G-o-o-d_bank_client");//good
        Assert.assertEquals(0, numberFails(customer, "login"));
        customer.setLogin("A___b...125-asw");//good
        Assert.assertEquals(0, numberFails(customer, "login"));
    }

    @Test
    public void passwordFailTest() {
        customer.setPassword(null);//null
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword(null);//null
        Assert.assertEquals(0, numberFails(customer, "password", CustomerCheck.class));
        customer.setPassword("");//empty
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("Asd@34");//short
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("Asd@ytrefA");//no digits
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("asdf$lklk123");//no upper case
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("ASD$14357FDE");//no lower case
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("asdfEW3452");//no special symbol
        Assert.assertEquals(1, numberFails(customer, "password"));
        customer.setPassword("asdf@1236ASFL:K:KPO121215454jkjhtrhgiuhtjirhQWEDSAQAZXlkmhltlh,gothkijogtk");//long
        Assert.assertEquals(1, numberFails(customer, "password"));
        //ok
        customer.setPassword("as@u12acgQCr");
        Assert.assertEquals(0, numberFails(customer, "password"));
        customer.setPassword("12345678aG!");
        Assert.assertEquals(0, numberFails(customer, "password"));
        customer.setPassword("!@#$%aS1");
        Assert.assertEquals(0, numberFails(customer, "password"));
    }

    private <T> int numberFails(T input, String field, Class<?>... groups) {
        Set<ConstraintViolation<T>> set = validator.validateProperty(input, field, groups);
        return set.size();
    }
}