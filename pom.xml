<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <packaging>war</packaging>

    <name>jBank</name>
    <groupId>com.mede.jbank</groupId>
    <artifactId>jBank</artifactId>
    <version>1.0-SNAPSHOT</version>

    <pluginRepositories>
        <pluginRepository>
            <id>alfresco</id>
            <name>alfresco</name>
            <url>https://artifacts.alfresco.com/nexus/content/repositories/public/</url>
        </pluginRepository>
    </pluginRepositories>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!--build quality checks-->
        <build.quality.skip>false</build.quality.skip>
        <build.checkstyle.fail>true</build.checkstyle.fail>
        <build.findbugs.fail>true</build.findbugs.fail>
        <build.jacoco.fail>true</build.jacoco.fail>

        <!--plugins-->
        <plugin-tomcat8-maven-version>3.0-r1756463</plugin-tomcat8-maven-version>
        <plugin-jetty-version>9.4.18.v20190429</plugin-jetty-version>
        <plugin-jacoco-version>0.8.3</plugin-jacoco-version>
        <plugin-spotbugs-version>3.1.11</plugin-spotbugs-version>
        <plugin-checkstyle-version>3.1.0</plugin-checkstyle-version>
        <aspectj-maven-plugin-version>1.11.1</aspectj-maven-plugin-version>

        <!--plugin dependencies-->
        <puppycrawl-checkstyle-version>8.20</puppycrawl-checkstyle-version>

        <!--plugin settings-->
        <jacoco-coverage>0.8</jacoco-coverage>
        <web-port>8080</web-port>
        <web-context-path>/</web-context-path>

        <!--runtime dependencies-->
        <aspectj-version>1.9.4</aspectj-version>
        <java-jwt-version>3.8.0</java-jwt-version>
        <jjwt-version>0.10.5</jjwt-version>
        <liquibase-core-version>3.6.3</liquibase-core-version>
        <javax-version>3.1.0</javax-version>
        <lombok-version>1.18.4</lombok-version>
        <hikariCP-version>3.2.0</hikariCP-version>
        <jdbi-version>3.7.1</jdbi-version>
        <postgresql-version>42.2.5</postgresql-version>
        <log4j-version>2.11.2</log4j-version>
        <mail-version>1.6.2</mail-version>
        <bootstrap-version>4.3.1</bootstrap-version>
        <validation-api-version>2.0.1.Final</validation-api-version>
        <hibernate-validator-version>6.0.15.Final</hibernate-validator-version>
        <javax.el-version>3.0.1-b10</javax.el-version>
        <bootstrup-version>4.3.1</bootstrup-version>
        <jquery-version>3.2.1</jquery-version>
        <jstl-version>1.2.5</jstl-version>
        <font-aweasome-version>4.1.0</font-aweasome-version>
        <webjars-datatables-vesion>1.10.19</webjars-datatables-vesion>

        <!--test dependencies-->
        <junit-version>4.12</junit-version>
        <mockito-version>1.9.5</mockito-version>
        <opentable-version>0.13.1</opentable-version>
        <javafaker-version>0.16</javafaker-version>

    </properties>

    <profiles>
        <profile>
            <id>unchecked</id>
            <activation>
                <property>
                    <name>skipTests</name>
                </property>
            </activation>
            <properties>
                <build.quality.skip>true</build.quality.skip>
            </properties>
        </profile>

        <profile>
            <id>nofail</id>
            <properties>
                <build.quality.skip>false</build.quality.skip>
                <build.checkstyle.fail>false</build.checkstyle.fail>
                <build.findbugs.fail>false</build.findbugs.fail>
                <build.jacoco.fail>false</build.jacoco.fail>
            </properties>
        </profile>

        <profile>
            <id>pipeline</id>
            <properties>
                <build.quality.skip>false</build.quality.skip>
                <build.checkstyle.fail>true</build.checkstyle.fail>
                <build.findbugs.fail>true</build.findbugs.fail>
                <!--TODO temporarily disabled due to lack of the test coverage-->
                <build.jacoco.fail>false</build.jacoco.fail>
            </properties>
        </profile>
    </profiles>

    <build>
        <finalName>jBank</finalName>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <configuration>
                        <skip>${build.quality.skip}</skip>
                        <haltOnFailure>${build.jacoco.fail}</haltOnFailure>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>report</id>
                            <phase>prepare-package</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>jacoco-check</id>
                            <goals>
                                <goal>check</goal>
                            </goals>
                            <configuration>
                                <rules>
                                    <rule>
                                        <element>PACKAGE</element>
                                        <limits>
                                            <limit>
                                                <counter>LINE</counter>
                                                <value>COVEREDRATIO</value>
                                                <minimum>${jacoco-coverage}</minimum>
                                            </limit>
                                        </limits>
                                        <excludes>
                                            <exclude>com.mede.jbank.entity</exclude>
                                            <exclude>com.mede.jbank.exception</exclude>
                                        </excludes>
                                    </rule>
                                </rules>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <configuration>
                        <skip>${build.quality.skip}</skip>
                        <failOnViolation>${build.checkstyle.fail}</failOnViolation>
                        <configLocation>config/checkstyle.xml</configLocation>
                        <encoding>UTF-8</encoding>
                        <consoleOutput>true</consoleOutput>
                        <includeTestSourceDirectory>true</includeTestSourceDirectory>
                    </configuration>
                    <executions>
                        <execution>
                            <id>validate</id>
                            <phase>validate</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                    <dependencies>
                        <dependency>
                            <groupId>com.puppycrawl.tools</groupId>
                            <artifactId>checkstyle</artifactId>
                            <version>${puppycrawl-checkstyle-version}</version>
                        </dependency>
                    </dependencies>
                </plugin>

                <plugin>
                    <groupId>com.github.spotbugs</groupId>
                    <artifactId>spotbugs-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>compile</id>
                            <phase>compile</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <skip>${build.quality.skip}</skip>
                        <failOnError>${build.findbugs.fail}</failOnError>
                        <effort>Max</effort>
                        <threshold>Low</threshold>
                        <xmlOutput>true</xmlOutput>
                        <excludeFilterFile>config/spotbugs.xml</excludeFilterFile>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>com.github.m50d</groupId>
                    <artifactId>aspectj-maven-plugin</artifactId>
                    <configuration>
                        <complianceLevel>${maven.compiler.source}</complianceLevel>
                        <source>${maven.compiler.source}</source>
                        <target>${maven.compiler.target}</target>
                        <showWeaveInfo>true</showWeaveInfo>
                        <verbose>true</verbose>
                        <Xlint>ignore</Xlint>
                        <encoding>${project.build.sourceEncoding}</encoding>
                        <!-- Important due to Lombok -->
                        <forceAjcCompile>true</forceAjcCompile>
                        <sources/>
                        <testSources/>
                        <weaveDirectories>
                            <weaveDirectory>${project.build.directory}/classes</weaveDirectory>
                        </weaveDirectories>
                        <excludes>
                            <exclude>**/*.java</exclude>
                        </excludes>
                        <testSources/>
                    </configuration>
                    <dependencies>
                        <dependency>
                            <groupId>org.aspectj</groupId>
                            <artifactId>aspectjrt</artifactId>
                            <version>${aspectj-version}</version>
                        </dependency>
                        <dependency>
                            <groupId>org.aspectj</groupId>
                            <artifactId>aspectjtools</artifactId>
                            <version>${aspectj-version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <id>default-compile</id>
                            <phase>process-classes</phase>
                            <goals>
                                <goal>compile</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

            </plugins>
        </pluginManagement>

        <plugins>

            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat8-maven-plugin</artifactId>
                <version>${plugin-tomcat8-maven-version}</version>
                <configuration>
                    <port>${web-port}</port>
                    <path>${web-context-path}</path>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>${plugin-jetty-version}</version>
                <configuration>
                    <httpConnector>
                        <port>${web-port}</port>
                    </httpConnector>
                    <webApp>
                        <contextPath>${web-context-path}</contextPath>
                    </webApp>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${plugin-jacoco-version}</version>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>${plugin-checkstyle-version}</version>
            </plugin>

            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>${plugin-spotbugs-version}</version>
            </plugin>

            <plugin>
                <groupId>com.github.m50d</groupId>
                <artifactId>aspectj-maven-plugin</artifactId>
                <version>${aspectj-maven-plugin-version}</version>
            </plugin>

        </plugins>

    </build>

    <dependencies>
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>${aspectj-version}</version>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>${javax-version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version>${hikariCP-version}</version>
        </dependency>

        <dependency>
            <groupId>org.jdbi</groupId>
            <artifactId>jdbi3-core</artifactId>
            <version>${jdbi-version}</version>
        </dependency>

        <dependency>
            <groupId>org.jdbi</groupId>
            <artifactId>jdbi3-sqlobject</artifactId>
            <version>${jdbi-version}</version>
        </dependency>

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>${postgresql-version}</version>
        </dependency>

        <dependency>
            <groupId>com.sun.mail</groupId>
            <artifactId>javax.mail</artifactId>
            <version>${mail-version}</version>
        </dependency>

        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>${validation-api-version}</version>
        </dependency>

        <dependency>
            <groupId>org.glassfish</groupId>
            <artifactId>javax.el</artifactId>
            <version>${javax.el-version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate-validator-version}</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok-version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-api</artifactId>
            <version>${jjwt-version}</version>
        </dependency>

        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-impl</artifactId>
            <version>${jjwt-version}</version>
        </dependency>

        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-jackson</artifactId>
            <version>${jjwt-version}</version>
        </dependency>

        <dependency>
            <groupId>org.liquibase</groupId>
            <artifactId>liquibase-core</artifactId>
            <version>${liquibase-core-version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>${log4j-version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j-version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.taglibs</groupId>
            <artifactId>taglibs-standard-spec</artifactId>
            <version>${jstl-version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.taglibs</groupId>
            <artifactId>taglibs-standard-impl</artifactId>
            <version>${jstl-version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>bootstrap</artifactId>
            <version>${bootstrap-version}</version>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>jquery</artifactId>
            <version>${jquery-version}</version>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>font-awesome</artifactId>
            <version>${font-aweasome-version}</version>
        </dependency>

        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>datatables</artifactId>
            <version>${webjars-datatables-vesion}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit-version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.jdbi</groupId>
            <artifactId>jdbi3-testing</artifactId>
            <version>${jdbi-version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.opentable.components</groupId>
            <artifactId>otj-pg-embedded</artifactId>
            <version>${opentable-version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.github.javafaker</groupId>
            <artifactId>javafaker</artifactId>
            <version>${javafaker-version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>${mockito-version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>
</project>
